# Application List: 

## JAVA Apps ##
* ETL Framework 
	* Hive and Impala ETL framework for Hadoop Datapipeline

![ETLworkflow.001.png](https://bitbucket.org/repo/5L4AXB/images/3626026867-ETLworkflow.001.png)

## JAVA API ##

* tpim-javautils.jar
	* CapacityLogging - API for logging file size, count in ETL Process
	* Config – API Reading and Parsing XML, CSV, property, JSON configuration files
	* Date - API for Date, Time conversion and formatting
	* HDFS - API for reading/writing in Hadoop File System as if it where a local file system
	* Impala - API for Impala JDBC faster than using shell command
	* JSCH - API for sftp, ssh
	* JSON - API for JSON file parsing, conversion to JSON
	* LocalFs - API for Local File System
	* Logging - API for Log4j logging
	* Mail - API for sending email
	* SingleInstance - API for making sure only 1 file runs
	* Zoomdata - API for sending Live Data to Zoomdata BI

# Getting Cleaned Up Apps: #
## JAVA Apps ##

* ComputeStats.jar - Hadoop Impala work around for DB query failures multi-thread query thru the impala Java API
* RefreshTables.jar - Hadoop Impala work around for DB to update data multi-thread query thru the impala Java API
* HdfsLoaderWatch.jar - A much faster version of the Perl HDFS Loader Watch.  Reading HDFS using Hadoop file system Java API
* HdfsMergePartitioned.jar - Hadoop Impala/Hive Portioned table file merge for faster query 
* HdfsMergeNonPart-1.2.2.jar - Hadoop Impala/Hive Non-Portioned table file merge for faster query running every 4 hours
* HdfsLocalfsCleanup.jar - Hadoop HDFS and Local Unix temporary files cleanup
* CronParser.jar - Reads and Parse All cron jobs in all hosts of the Hadoop cluster and collection Cluster
	
## Scala ##
* Spark Elastic Search data load
	* Spark Application for Elastic Search Index loading
* Spark Web Log Parser
	* Spark Parsing Apache weblog
* Spark Web Log Sessionization.

## PERL Apps ##

* PartitioningMgr.pl - Create/Delete/Cleanup Hadoop Hive/Impala HDFS Partition TPIM Adaptors (like Mavenir, NOKLTE, Mavenir, etc.) according to configured retentions
* TMORACREPORTToHive.pl - Hadoop Impala/Hive/HDFS ETL for TPIM RAC Servers Data
* TmoColCapToHive.pl - Hadoop Impala/Hive/HDFS ETL for TPIM Collection Servers Capacity Data
* TmoColSysStatsToHive.pl - Hadoop Impala/Hive/HDFS ETL for TPIM Collection Servers System Statistics Data
* HdfsLdrWatch.pl - Hadoop HDFS Adaptor File Loading Monitoring.  Monitors TPIM Adaptor for HDFS File Count and Volume for operation monitoring
* swDevBackup.pl - Development Server Backup of Development files (individual user scripts, tools, files) to Backup server and to Hadoop HDFS
* RefreshCache.pl - Zoomdata BI work around to update data in Dashboard