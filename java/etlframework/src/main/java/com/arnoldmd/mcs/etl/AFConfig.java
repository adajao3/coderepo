package com.arnoldmd.mcs.etl;

import java.io.File;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * ETL Framework Configuration
 * @author dajaar01
 *
 */
public class AFConfig {

	private String _oracleUrl;
	private String _orausername;
	private String _orapassword; // read only password
	private String _jdbcUrl;
	private String _hiveUrl;
	private String _driverName;
	private String _dbName;
	private String _impalaShell;
	private String _processBy;
	private String _aggregationProcess;
	private String _currentDate;
	private String _aggregationType;

	public AFConfig() {

	}
/**
 * Create ETL Framework Config
 * @param oracleUrl
 * @param orausername
 * @param orapassword
 * @param impalaUrl
 * @param hiveUrl
 * @param dbName
 * @param impalaShell
 * @param processBy
 */
	public AFConfig(String oracleUrl, String orausername, String orapassword,
			String impalaUrl, String hiveUrl, String dbName, String impalaShell,String processBy) {
		this._oracleUrl = oracleUrl;
		this._orausername = orausername;
		this._orapassword = orapassword;
		this._jdbcUrl = impalaUrl;
		this._hiveUrl = hiveUrl;
		this._dbName = dbName;
		this._impalaShell = impalaShell;
		this._processBy = processBy; //impala hive spark
		
	}

	/**
	 * Read ETL Framework Configuration from config file
	 * @param ConfigFile
	 */
	public AFConfig(String ConfigFile) {
		Config config = ConfigFactory.parseFile(new File(ConfigFile));
		String sys = config.getString("cdh.sys"); //prod, preprod dev uat qa
		
		this._dbName = config.getString(sys + ".db");
		this._oracleUrl = config.getString(sys + ".oracleUrl"); 
		this._orausername = config.getString(sys + ".orausername"); 
		this._orapassword = config.getString(sys + ".orapassword");
		this._jdbcUrl = config.getString(sys + ".impalaUrl");
		this._hiveUrl = config.getString(sys + ".hiveUrl");
		this._driverName = config.getString(sys + ".driverName");
		this._dbName = config.getString(sys + ".dbName");
		this._impalaShell = config.getString(sys + ".impalaShell");
		this._aggregationProcess = config.getString(sys + ".aggregationProcess");
		this._currentDate = config.getString(sys + ".currentDate");
	}

	public String get_oracleUrl() {
		return _oracleUrl;
	}

	public void set_oracleUrl(String _oracleUrl) {
		this._oracleUrl = _oracleUrl;
	}

	public String get_orausername() {
		return _orausername;
	}

	public void set_orausername(String _orausername) {
		this._orausername = _orausername;
	}

	public String get_orapassword() {
		return _orapassword;
	}

	public void set_orapassword(String _orapassword) {
		this._orapassword = _orapassword;
	}

	public String get_jdbcUrl() {
		return _jdbcUrl;
	}

	public void set_jdbcUrl(String _impalaUrl) {
		this._jdbcUrl = _impalaUrl;
	}

	public String get_dbName() {
		return _dbName;
	}

	public void set_dbName(String _dbName) {
		this._dbName = _dbName;
	}

	public String get_impalaShell() {
		return _impalaShell;
	}

	public void set_impalaShell(String _impalaShell) {
		this._impalaShell = _impalaShell;
	}

	public String get_hiveUrl() {
		return _hiveUrl;
	}

	public void set_hiveUrl(String _hiveUrl) {
		this._hiveUrl = _hiveUrl;
	}

	public String get_processBy() {
		return _processBy;
	}

	public void set_processBy(String _processBy) {
		this._processBy = _processBy;
	}

	public String get_aggregationProcess() {
		return _aggregationProcess;
	}

	public void set_aggregationProcess(String _aggregationProcess) {
		this._aggregationProcess = _aggregationProcess;
	}

	public String get_currentDate() {
		return _currentDate;
	}

	public void set_currentDate(String _currentDate) {
		this._currentDate = _currentDate;
	}

	public String get_aggregationType() {
		return _aggregationType;
	}

	public void set_aggregationType(String _aggregationType) {
		this._aggregationType = _aggregationType;
	}

	public String get_driverName() {
		return _driverName;
	}

	public void set_driverName(String _driverName) {
		this._driverName = _driverName;
	}

}
