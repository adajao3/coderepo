package com.arnoldmd.mcs.etl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

/**
 * ETL Framework for Hadoop using Hive/Impala
 * 
 * @author dajaar01
 */
public class ETLFramework {
	private final static String APP_NAME = ETLFramework.class.getSimpleName();
	private static Logger log = Logger.getLogger(APP_NAME);
	private static Boolean executionStatus = true;
	private static AFConfig afg;

	public static void main(String[] args) {
		// get config
		ETLFramework aggFw = new ETLFramework();
		String confFile = aggFw.getConfig(args);
		afg = new AFConfig(confFile);
		// insert processing logs
		try {
			updateLogsTable();
			// get sql from ora control table, exec jdbc (hive Impala)
			RunETL();
		} catch (SQLException e) {
			log.fatal("Error executing: " + e);
		} catch (ClassNotFoundException e) {
			log.fatal("Error executing: " + e);
		}

	}

	/**
	 * Execute Queries form control Table using Hive or Impala JDBC
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void RunETL() throws ClassNotFoundException, SQLException {

		LinkedHashMap<String, String> aggregationSqls = new LinkedHashMap<String, String>();
		OracleQuery etlDbOp = new OracleQuery();
		String aggregationProcess = afg.get_aggregationProcess();
		String aggregationType = afg.get_aggregationType();

		OracleDbConnectivity oraCon = new OracleDbConnectivity(
				afg.get_oracleUrl(), afg.get_orausername(),
				afg.get_orapassword());
		// get sqls from oracle control table

		aggregationSqls = etlDbOp.getAggregationSqls_Agg(oraCon,
				aggregationType, aggregationProcess);

		//
		if (aggregationSqls.size() == 0) {
			log.fatal("No sqls stored for aggregation type " + aggregationType
					+ ".");

			etlDbOp.updateAggregationLog_Agg(oraCon, 0, "", aggregationType,
					"Failed", "", "No sqls stored for aggregation type "
							+ aggregationType + ".");
			System.exit(1);
		}

		Iterator<Entry<String, String>> it = aggregationSqls.entrySet()
				.iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			log.info("#Started loading : " + pair.getKey() + "#");
			log.info("Executing :" + pair.getValue());

			if (!aggregationProcess.equalsIgnoreCase("Reprocess")) {
				etlDbOp.updateAggregationLog_Agg(oraCon, 1, pair.getKey()
						.toString(), aggregationType, "Started",
						"jdbc" + pair.getValue(), "null");
			} else {

				etlDbOp.updateAggregationLog_Agg(oraCon, 0, pair.getKey()
						.toString(), aggregationType, "Started",
						"jdbc" + pair.getValue(), "null");

			}
			long startTime = System.currentTimeMillis();

			String driverName = afg.get_driverName();
			CdhQuery cdq = new CdhQuery();
			Connection con = cdq.getConnection(driverName, afg.get_jdbcUrl());

			boolean executionStatus = cdq.CdhExecInsert(con, pair.getValue()
					.toString());

			if (executionStatus) {
				long endTime = System.currentTimeMillis();

				double timeTaken = (endTime - startTime) / 1000.0;
				log.info("Inserted  in " + timeTaken + "s.");
				log.info("Execution status :Success");
				if (!aggregationProcess.equalsIgnoreCase("Reprocess")) {

					etlDbOp.updateAggregationLog_Agg(oraCon, 1, pair.getKey()
							.toString(), aggregationType, "Completed", "jdbc"
							+ pair.getValue(), "null");

				} else {

					etlDbOp.updateAggregationLog_Agg(oraCon, 0, pair.getKey()
							.toString(), aggregationType, "Completed", "jdbc"
							+ pair.getValue(), "null");

				}

				log.info("#Completed loading : " + pair.getKey() + "#");
			} else {
				log.info("Execution status :Failure");
				if (!aggregationProcess.equalsIgnoreCase("Reprocess")) {

					etlDbOp.updateAggregationLog_Agg(oraCon, 1, pair.getKey()
							.toString(), aggregationType, "Failed", "jdbc"
							+ pair.getValue(), "null");
					log.fatal("Execution status :Failed");
				} else {
					etlDbOp.updateAggregationLog_Agg(oraCon, 0, pair.getKey()
							.toString(), aggregationType, "Failed", "jdbc"
							+ pair.getValue(), "null");
					log.fatal("Execution status :Failed");
				}

				System.exit(1);
			}

		}

	}

	/**
	 * Get Configuration File
	 * 
	 * @return configuration filename
	 * @exception parseexception
	 */
	public String getConfig(String[] args) {
		Options opt = new Options();
		opt.addOption("c", "config", true, "[filename] for config file");
		CommandLineParser cliparser = new BasicParser();
		CommandLine cli = null;
		try {
			cli = cliparser.parse(opt, args);

		} catch (ParseException e) {
			printusage(opt);

		}
		if (cli.hasOption("c")) {
			return (cli.getOptionValue("c"));
		} else {
			printusage(opt);
		}
		executionStatus = false;
		return null;
	}

	/**
	 * Print Application Usage
	 * 
	 * @param opt
	 */
	private static void printusage(Options opt) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(APP_NAME, opt);
	}

	/**
	 * Update Table Logs
	 * @throws SQLException
	 */
	private static void updateLogsTable() throws SQLException {

		String aggregationProcess = afg.get_aggregationProcess();
		String currentDate = afg.get_currentDate();
		String dbName = afg.get_dbName();
		String aggregationType = afg.get_aggregationType();
		String driverName = afg.get_driverName();
		CdhQuery cdq = new CdhQuery();
		Connection con = cdq.getConnection(driverName, afg.get_jdbcUrl());

		if (aggregationProcess.equalsIgnoreCase("Weekly")
				|| aggregationProcess.equalsIgnoreCase("Rerun")) {
			HashMap<String, Integer> weekIds = getWeekAndDateIds(currentDate,
					dbName, aggregationProcess, con);

			String aggCmd = "\"insert into " + dbName
					+ ".aggregation_input values (" + weekIds.get("WeekID")
					+ "," + weekIds.get("PreviousWeekID") + ","
					+ weekIds.get("StartDateID") + ","
					+ weekIds.get("EndDateID") + ","
					+ weekIds.get("WeekNumber") + "," + weekIds.get("Year")
					+ "," + weekIds.get("PreviousYearLastWeek") + ",'"
					+ aggregationProcess + "'," + 0 + ",'" + aggregationType
					+ "',now())\"";
			log.debug("inserting into aggregation_input");

			try {
				cdq.CdhExecSql(con, aggCmd);

				if (executionStatus) {
					log.info("aggregation_input table updated successfully.");
					log.info("#Starting aggregation process for week :"
							+ weekIds.get("WeekID") + " aggregation type :"
							+ aggregationType + " and aggregation process :"
							+ aggregationProcess + "#");

				} else {
					log.fatal("Error occured while updating aggregation_input table.");
					log.fatal("aggrgation process stopped.");
					System.exit(1);
				}
			} catch (SQLException e) {
				log.fatal("Error in execution: " + e);
			}
		}

	}

	/**
	 * Get Weekid and Date
	 * @param date
	 * @param dbName database name
	 * @param aggregationProcess weekly, daily rerun
	 * @param con JDBC Connection 
	 * @return
	 */
	private static HashMap<String, Integer> getWeekAndDateIds(String date,
			String dbName, String aggregationProcess, Connection con) {

		HashMap<String, Integer> weekIds = new HashMap<String, Integer>();

		Statement stmtObj = null;
		ResultSet rsObj = null;

		int weekid = 0;
		int previousWeekId = 0;
		// Connection con = null;
		try {
			// con = conObj.getConnection();
			stmtObj = con.createStatement();

			String query = "select weekid,PreviousWeekID from " + dbName
					+ ".di_date where datename = '" + date + "'";
			System.out.println("Executing :" + query);
			rsObj = stmtObj.executeQuery(query);

			while (rsObj.next()) {

				weekid = rsObj.getInt("weekid");
				previousWeekId = rsObj.getInt("previousweekid");
				System.out.println("Weekid :" + weekid);

				if (aggregationProcess.equalsIgnoreCase("Weekly")) {
					weekid = previousWeekId;
					weekIds.put("WeekID", weekid);
					System.out.println("Weekid :" + weekid);
				} else {
					weekIds.put("WeekID", rsObj.getInt("weekid"));
					System.out.println("Weekid :" + rsObj.getInt("weekid"));
				}
			}
			query = "select max(datename) as enddate,min(datename) as startdate, max(dateid) as dateid, min(dateid) as StartDateID, PreviousWeekID as PreviousWeekID,WeekNum as WeekNumber, YearID as Year  from "
					+ dbName
					+ ".di_date where weekid = "
					+ weekIds.get("WeekID")
					+ " group by weekid,PreviousWeekID,WeekNumber,Year";
			System.out.println("Executing :" + query);
			rsObj = stmtObj.executeQuery(query);

			while (rsObj.next()) {
				weekIds.put("EndDateID", rsObj.getInt("dateid"));
				weekIds.put("StartDateID", rsObj.getInt("startdateid"));
				weekIds.put("PreviousWeekID", rsObj.getInt("previousweekid"));
				weekIds.put("WeekNumber", rsObj.getInt("weeknumber"));
				weekIds.put("Year", rsObj.getInt("year"));

			}
			rsObj = null;
			String query1 = "select max(weekid) as previousyearlastweek from "
					+ dbName + ".di_date where yearid=(select yearid-1 from "
					+ dbName + ".di_date where weekid=201521 limit 1)";
			rsObj = stmtObj.executeQuery(query1);

			while (rsObj.next()) {
				weekIds.put("PreviousYearLastWeek",
						rsObj.getInt("previousyearlastweek"));
			}

			log.debug("DateID :" + weekIds.get("EndDateID"));
			log.debug("StartDateID :" + weekIds.get("StartDateID"));
			log.debug("PreviousWeekID :" + weekIds.get("PreviousWeekID"));
			log.debug("Weeknumber :" + weekIds.get("WeekNumber"));
			log.debug("Year :" + weekIds.get("Year"));
			log.debug("PreviousYearLastWeek :"
					+ weekIds.get("PreviousYearLastWeek"));
			log.debug("End of Resultset %%%%%%%%%%%%%%%%%%%%%%%%%%%%");

		} catch (SQLException sqlExpnObj) {
			sqlExpnObj.printStackTrace();
		} catch (Exception expnObj) {
			expnObj.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return weekIds;
	}

}
