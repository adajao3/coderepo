package com.arnoldmd.mcs.etl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * Utility to run Hive and Impala JDBC Query
 * 
 * @author dajaar01
 *
 */
public class CdhQuery {

	private final static String APP_NAME = ETLFramework.class.getSimpleName();
	private static Logger log = Logger.getLogger(APP_NAME);

	/**
	 * Get JDBC connection for Impala or Hive
	 * 
	 * @param driverName
	 *            the driver name for Impala or Hive
	 * @param jdbcUrl
	 *            the JDBC url for Impala or Hive
	 * @return Connection
	 * @throws SQLException
	 */
	public Connection getConnection(String driverName, String jdbcUrl)
			throws SQLException {
		try {
			// Register driver and create driver instance
			Class.forName(driverName);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			log.fatal("Error Connecting to " + jdbcUrl, ex);
			return null;
		}

		// get connection
		log.debug("Running SQL on: " + jdbcUrl);
		Connection con = DriverManager.getConnection(jdbcUrl);
		log.debug("connected to: " + jdbcUrl);
		System.out.println("connected");
		return con;
	}

	/**
	 * Execute Single SQL command
	 * 
	 * @param con
	 *            Connection
	 * @param SqlCmd
	 *            Single SQL command to be executed
	 * @return List of Strings as result set
	 * @throws SQLException
	 */
	public ArrayList<String> CdhExecSql(Connection con, String SqlCmd)
			throws SQLException {

		// create statement
		Statement stmt = con.createStatement();
		// execute statement
		ResultSet rs = stmt.executeQuery(SqlCmd);

		// Stores properties of a ResultSet object, including column count
		ResultSetMetaData metadata = rs.getMetaData();
		int colCount = metadata.getColumnCount();

		ArrayList<String> ResultList = new ArrayList<String>(colCount);
		while (rs.next()) {
			int i = 1;
			while (i <= colCount) {
				ResultList.add(rs.getString(i++));
			}
		}

		con.close();
		return ResultList;

	}

	/**
	 * 
	 * Execute List SQL command
	 * 
	 * @param con
	 *            tConnection
	 * @param SqlCmd
	 *            Single SQL command to be executed
	 * @return List of Strings as result set
	 * @throws SQLException
	 */
	public ArrayList<String> CdhExecSqlList(Connection con,
			ArrayList<String> SqlCmdList) throws SQLException {

		ArrayList<String> ResultList = new ArrayList<String>();

		for (String sqlCmd : SqlCmdList) {
			ResultList.addAll(CdhExecSql(con, sqlCmd));
		}
		return ResultList;
	}

	public Boolean CdhExecInsert(Connection con, String SqlCmd) {

		Boolean result = false;

		// create statement
		Statement stmt;
		try {
			stmt = con.createStatement();
			stmt.executeQuery(SqlCmd);
			result = true;
		} catch (SQLException e) {
			log.fatal("Error excuting SQL: " + SqlCmd);
		}

		return result;
	}
}
