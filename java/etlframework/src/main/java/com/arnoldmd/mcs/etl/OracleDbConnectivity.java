package com.arnoldmd.mcs.etl;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 * Use for SQL saved in Oracle Table
 * @author dajaar01
 *
 */
public class OracleDbConnectivity {

	public String url = "";
	public String userName = "";
	public String password = "";
	Connection con = null;

	/**
	 * 
	 * @param url
	 * @param userName
	 * @param password
	 */
	public OracleDbConnectivity(String url, String userName, String password) {
		this.url = url;
		this.userName = userName;
		this.password = password;
	}

	/**
	 * get Oracle DB Connection
	 * @return
	 */
	public Connection getConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			con = DriverManager.getConnection(this.url, this.userName,
					this.password);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.con;
	}

	/**
	 * Close Connection
	 */
	public void closeConnection() {
		try {
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
