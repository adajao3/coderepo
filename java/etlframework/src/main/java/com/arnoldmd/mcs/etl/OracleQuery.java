package com.arnoldmd.mcs.etl;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.LinkedHashMap;



public class OracleQuery {

	
	public void updateAggregationLog_Agg(OracleDbConnectivity conn,int weekid, String tableName, String aggregationType, String status, String impalaCommand, String errorDesc) throws ClassNotFoundException, SQLException {
		
		String qry = "insert into aggregation_log values("+weekid+", '"+tableName+"', '"+aggregationType+"', '"+status+"', '"+impalaCommand.replaceAll("'","''")+"','"+errorDesc+"', SYSDATE)";
		System.out.println("Log entry :"+impalaCommand);
		PreparedStatement prest;
		Connection con = null;
		try {
			con = conn.getConnection();
			prest = con.prepareStatement(qry);
			prest.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally
		{
			try {
				con.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		
		
	}
	
	public LinkedHashMap<String, String> getAggregationSqls_Agg(OracleDbConnectivity conn,String aggregationType, String aggregationProcess) throws ClassNotFoundException, SQLException {
		
		String qry = "select table_name,sql from aggregation_sql where aggregation_type = '"+aggregationType+"' and aggregation_process = '"+aggregationProcess+"' order by ORDER_OF_EXECUTION";
		System.out.println("Fetching sqls :"+qry);
		LinkedHashMap<String, String> aggregationSqls = new LinkedHashMap<String, String>();
		Connection con = null;
		PreparedStatement prest;
		try {
			con = conn.getConnection();
			prest = con.prepareStatement(qry);
			ResultSet rs = prest.executeQuery();
			while(rs.next()){
				aggregationSqls.put(rs.getString("table_name"),rs.getString("sql"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally
		{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return aggregationSqls;
	}

}
