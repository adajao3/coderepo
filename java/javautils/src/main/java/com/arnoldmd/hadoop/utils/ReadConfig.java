/*
 * Copyright 2014 ArnoldMD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/24/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/ReadConfig.java,v 1.2
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:    $Log: ReadConfig.java,v $
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:    Mulitple updates
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:   
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:    Revision 1.1 2014/12/09
 * 18:25:49 arnoldmd 2014/11/25 02:22:47 arnoldmd Exp $    Log:    updated Mail
 * and HDFS 2014/11/25 02:22:47 arnoldmd Exp $    Log:      Log:    Revision
 * 1.2 2014/11/25 02:22:47 arnoldmd   Log:    Update for HDFS utilities amd
 * merging   Log:   
 */
package com.arnoldmd.hadoop.utils;

import com.arnoldmd.hadoop.system.SystemUtils;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author adajao2
 */
public class ReadConfig {

    private static final String APP_SVN_REV = "$Version: 1.0$".replaceAll("\\$", "").replace("sion", "") + " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
    private static final String APP_NAME = ReadConfig.class.getSimpleName();
    private static Logger log = Logger.getLogger(APP_NAME);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//            Logger.getLogger(ReadConfig.class.getName()).log(Level.SEVERE, null, ex);

        Map<String, String> confMap = getConfigMap();
    }

    /**
     *
     */
    private static String confFile_ = "";
    private static String tpimConfPath_ = "";
    private static String tpimMapFile_ = "";
    private static String logginPropertiesFile_ = "";

    /**
     *
     */
    public static void readConf() {
        getAdaptorFile();
    }

    /**
     *
     * @return @throws FileNotFoundException
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static ArrayList<Adaptor> getAdaptorConfigOld() throws FileNotFoundException, IOException, IllegalAccessException {
        Map<String, String> confMapAll = new HashMap<String, String>();
//        Map<String, String> confMap = new HashMap<String, String>();
        List<String> adaptorList = new ArrayList<String>();
        ArrayList<Adaptor> adaptorArrayList = new ArrayList<Adaptor>();
        String pattern = "(.*)_HDFS_DIR";
        Pattern adpPattern = Pattern.compile(pattern);
        String confKey = "";
        String confValue = "";

        try (BufferedReader br = new BufferedReader(new FileReader(confFile_))) {
            //map for fpetans
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains("=")) {
                    String[] lineSplit = line.split("=");
                    confKey = lineSplit[0];
                    confValue = lineSplit[1];

                    if (confKey.contains("TPIM_ADAPTORFILE") && !tpimConfPath_.isEmpty()) {
                        String tpimAdaptorFile = FilenameUtils.getName(confValue);
                        confMapAll.put(confKey, tpimConfPath_ + tpimAdaptorFile);
                    } else {
                        confMapAll.put(confKey, confValue);
                        Matcher adpM = adpPattern.matcher(confKey);
                        if (adpM.find()) {
                            String strAdaptor = adpM.group(1).contains("TPIM_PMDATA") ? "TPIM" : adpM.group(1);
                            adaptorList.add(strAdaptor);
//                            System.out.println(strAdaptor);
                            log.debug(APP_SVN_REV + "strAdaptor " + strAdaptor);
                        }
                    }
                }
            }
        }
        //get adaptor
        // line is not visible here.

        Map<String, String> adaptorMap = getAdaptorMap();
        for (String s : adaptorList) {
            //Adaptor adaptor = new Adaptor();
            Adaptor adaptor = new Adaptor();
            adaptor.setName(s);

            for (String key : adaptorMap.keySet()) {
                String configValueTmp = confMapAll.get(s + key);

                if (StringUtils.isNotEmpty(configValueTmp)) {
                    String configValue = getConfigValue(confMapAll, configValueTmp);
                    String adaptProp = adaptorMap.get(key);

                    try {
                        String objType = PropertyUtils.getPropertyType(adaptor, adaptProp).toString();
                        if (objType.contains("String")) {
                            PropertyUtils.setProperty(adaptor, adaptProp, configValue);
                        } else {
                            PropertyUtils.setProperty(adaptor, adaptProp, Integer.parseInt(configValue));

                        }
                    } catch (InvocationTargetException ex) {
                        log.fatal(APP_SVN_REV + " InvocationTargetException " + ex);

                    } catch (NoSuchMethodException ex) {
                        log.fatal(APP_SVN_REV + " NoSuchMethodException " + ex);
                    }
                }

            }
            adaptorArrayList.add(adaptor);
        }
        return adaptorArrayList;
    }

    /**
     *
     */
    public static void getAdaptorFile() {
        SystemUtils os = new SystemUtils();
        if (os.isWindows()) {
            //read from ftp?
            confFile_ = "C:\\Temp\\WestPreProduction.cf";
            tpimConfPath_ = "C:\\Temp\\PreProduction_Adaptor.cf";
            tpimMapFile_ = "C:\\Temp\\omes_tpim_name_mapping_v2_amd.csv";
            logginPropertiesFile_ = "mylogging.properties";
            log.info("confFile: " + confFile_);
            log.info("tpimConfPath: " + tpimConfPath_);
            log.info("tpimMapFile: " + tpimMapFile_);
            log.info("logginPropertiesFile: " + logginPropertiesFile_);
        } else {
            //read from unix
            confFile_ = "/home/omc/staging/etl/config/WestPreProduction.cf";
            tpimConfPath_ = "";
            tpimMapFile_ = "/home/omc/staging/etl/java/bin/omes_tpim_name_mapping_v2_amd.csv";
            logginPropertiesFile_ = "mylogging.properties";
            log.info("confFile: " + confFile_);
            log.info("tpimConfPath: " + tpimConfPath_);
            log.info("tpimMapFile: " + tpimMapFile_);
            log.info("logginPropertiesFile: " + logginPropertiesFile_);
        }
    }

    public static void getAdaptorTestFile() {
        SystemUtils os = new SystemUtils();
        if (os.isWindows()) {
            //read from ftp?
            confFile_ = "C:\\Temp\\WestPreProduction.cf";
            tpimConfPath_ = "C:\\Temp\\PreProduction_Adaptor.cf";
            tpimMapFile_ = "C:\\Temp\\omes_tpim_name_mapping_v2_amd.csv";
            logginPropertiesFile_ = "mylogging.properties";
            log.info("confFile: " + confFile_);
            log.info("tpimConfPath: " + tpimConfPath_);
            log.info("tpimMapFile: " + tpimMapFile_);
            log.info("logginPropertiesFile: " + logginPropertiesFile_);
        } else {
            //read from unix
            confFile_ = "/home/omc/staging/etl/java/WestPreProduction.cf";
            tpimConfPath_ = "";
            tpimMapFile_ = "/home/omc/staging/etl/java/bin/omes_tpim_name_mapping_v2_amd.csv";
            logginPropertiesFile_ = "mylogging.properties";
            log.info("confFile: " + confFile_);
            log.info("tpimConfPath: " + tpimConfPath_);
            log.info("tpimMapFile: " + tpimMapFile_);
            log.info("logginPropertiesFile: " + logginPropertiesFile_);
        }
    }

    /**
     *
     * @return AdaptorMap
     */
    public static Map<String, String> getAdaptorMap() {
        Map<String, String> adaptorMap = new HashMap<String, String>();
        adaptorMap.put("_DIR_IMPORT", "importPath");
        adaptorMap.put("_HEADER", "header");
        adaptorMap.put("_TABLES", "tables");
        adaptorMap.put("_HDFS_DIR", "hdfsPath");
        adaptorMap.put("_RETENTION", "retention");
        adaptorMap.put("_COLUMNS", "columns");
        adaptorMap.put("_TRACE", "trace");
        adaptorMap.put("_PARTITIONING", "partitioning");
        adaptorMap.put("_RAWSQL", "rawSql");
        adaptorMap.put("_NOMERGE", "nomerge");
        adaptorMap.put("_MERGED", "merged");

        return adaptorMap;
    }

    /**
     *
     * @param confMapAll
     * @param confValue
     * @return ConfigValue
     */
    public static String getConfigValue(Map<String, String> confMapAll, String confValue) {
//        System.out.println("confValue: " + confValue);
        log.info("confValue: " + confValue);
//        String pattern = "(.*)<(.*)>(.*)";
        Pattern fkPetan = Pattern.compile("(<\\w+>)");
        Matcher m = fkPetan.matcher(confValue);

//        int count = 0;
        if (m.find()) {
//            count++;
//            System.out.println("found: " + count + " : "
//                    + m.start() + " - " + m.end() + " " + m.group() + ": " + getFkPetan(m.group()));
            String fkPetanVal = confMapAll.get(getFkPetan(m.group()));
//            System.out.println("color2: " + fkPetanVal);
            String confValue1 = m.replaceFirst(fkPetanVal);
//            System.out.println("confValue: " + confValue1);
            log.trace("fkPetanVal " + fkPetanVal);
            log.trace(APP_SVN_REV + "confValue " + confValue1);
            confValue = getConfigValue(confMapAll, confValue1);
        }

        return confValue;

    }

    /**
     *
     * @param fkPetan
     * @return Petan
     */
    public static String getFkPetan(String fkPetan) {
//        System.out.println("fkPetan: " + fkPetan);
        Pattern pattern = Pattern.compile("^<(.*)>$");
        Matcher m = pattern.matcher(fkPetan);
        if (m.find()) {
//            System.out.println("m.group " + m.group(1));
            log.trace(APP_SVN_REV + "m.group " + m.group(1));
            return m.group(1);
        }
        return fkPetan;
    }

    /**
     * Returns Hashmap of the config file with the delimiter by defualt "# in
     * the beginning of the file is ignored
     *
     * @param delimiter delimiter use in the config file
     * @param configFile full path name of the config file
     * @return Map<String, String> Hasmap of the config file
     * @throws FileNotFoundException
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static Map<String, String> getConfigMap(String delimiter, String configFile) throws FileNotFoundException, IOException, IllegalAccessException {
//        System.out.println("String delimiter: " + delimiter + " , String configFile: " + configFile);
        log.debug(APP_SVN_REV + " String delimiter: " + delimiter + " , String configFile: " + configFile);
        Map<String, String> confMap = new HashMap<String, String>();
//        Map<String, String> confMap = new HashMap<String, String>();
        String confKey = "";
        String confValue = "";
        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {
            //map for fpetans
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains("=") && !line.startsWith("#")) {

                    String[] lineSplit = line.split("=");
                    confKey = lineSplit[0];
                    confValue = lineSplit[1];
                    if (confMap.containsKey(confKey)) {
                        //key already exist
//                        System.out.println("ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                        log.trace(APP_SVN_REV + " " + "ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);

                    } else {
                        confMap.put(confKey, confValue);
                        log.trace(APP_SVN_REV + " confKey " + confKey + " confValue " + confValue);
//                        System.out.println("confKey " + confKey + " confValue " + confValue);

                    }
                }
            }

        }
        return confMap;
    }

    /**
     * Returns Hashmap of the config file with the delimiter by defualt "# in
     * the beginning of the file is ignored
     *
     * Default delimiter is "=" No configFile full path name of the config file
     * it will read default config file
     * "/home/omc/staging/etl/config/WestPreProduction.cf"
     *
     * @return Map<String, String> Hasmap of the config file
     * @throws FileNotFoundException
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static Map<String, String> getConfigMap() {
//        System.out.println("getAdaptorFile :done2!");
        getAdaptorFile();
        String configFile = confFile_;
        String delimiter = "=";
        log.debug("String delimiter: " + delimiter + " , String configFile: " + configFile);
        Map<String, String> confMap = new HashMap<>();
        Map<String, String> newConfMap = new HashMap<>();

//        Map<String, String> confMap = new HashMap<String, String>();
        String confKey = "";
        String confValue = "";
        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {
            //map for fpetans
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains("=") && !line.startsWith("#")) {

                    String[] lineSplit = line.split("=");
                    confKey = lineSplit[0];
                    confValue = lineSplit[1];
                    if (confKey.contains("TPIM_ADAPTORFILE")) {
                        if (tpimConfPath_.isEmpty()) {
                            tpimConfPath_ = confValue;
                        }
//                        System.out.println("tpimConfPath: " + confValue);
                        log.trace("tpimConfPath: " + confValue);
                    }

                    if (confMap.containsKey(confKey)) {
                        //key already exist
//                        System.out.println("ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                        log.warn(APP_SVN_REV + " " + "ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                    } else {
                        confMap.put(confKey, confValue);
//                        Debug.println("confKey " + confKey, "confValue " + confValue);
//                        System.out.println("confKey " + confKey + " confValue " + confValue);
                        log.trace(APP_SVN_REV + " " + "confKey " + confKey + "confValue " + confValue);
                    }
                }

            }
            //TODO replace petan
            newConfMap = replaceFkPetan(confMap);
            //TODO ADD TPIM
            String tpim_hdfsDir = newConfMap.get("TPIM_PMDATA_HDFS_DIR");
            String tpim_retention = newConfMap.get("TPIM_RETENTION");
            Map<String, String> tpimConfMap = getConfigTpimMap(tpim_hdfsDir, tpim_retention);
//            for (Map.Entry<String, String> entry : newConfMap.entrySet()) {
//                System.out.printf("before putall : %s and Value: %s %n", entry.getKey(), entry.getValue());
//            }
            newConfMap.putAll(tpimConfMap);
            for (Map.Entry<String, String> entry : newConfMap.entrySet()) {
//                System.out.printf("Final Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
//                log.debug("Final Key : " + entry.getKey() + " and Value: " + entry.getValue());
            }

        } catch (FileNotFoundException ex) {
            log.fatal("ERROR: getConfigMap FileNotFoundException" + ex);
//            ex.printStackTrace();
        } catch (IOException ex) {
            log.fatal("ERROR: getConfigMap IOException" + ex);
        }
        return newConfMap;
    }

    /**
     * Returns Hashmap of the config file with the delimiter by defualt "# in
     * the beginning of the file is ignored
     *
     * Default delimiter is "=" No configFile full path name of the config file
     * it will read default config file
     * "/home/omc/staging/etl/config/WestPreProduction.cf"
     *
     * @return Map<String, String> Hasmap of the config file
     * @throws FileNotFoundException
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static Map<String, String> getAdaptorConfigMap(String adaptor, String configFile) {
//        System.out.println("getAdaptorFile :done2!");
        getAdaptorTestFile();
        if (configFile.isEmpty()) {
            configFile = confFile_;
        }

        String delimiter = "=";
        log.debug("String delimiter: " + delimiter + " , String configFile: " + configFile);
        Map<String, String> confMap = new HashMap<>();
        Map<String, String> newConfMap = new HashMap<>();
        Map<String, String> filteredConfMap = new HashMap();

//        Map<String, String> confMap = new HashMap<String, String>();
        String confKey = "";
        String confValue = "";
        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {
            //map for fpetans
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains("=") && !line.startsWith("#")) {

                    String[] lineSplit = line.split("=");
                    confKey = lineSplit[0];
                    confValue = lineSplit[1];
                    if (confKey.contains("TPIM_ADAPTORFILE")) {
                        if (tpimConfPath_.isEmpty()) {
                            tpimConfPath_ = confValue;
                        }
//                        System.out.println("tpimConfPath: " + confValue);
                        log.trace("tpimConfPath: " + confValue);
                    }

                    if (confMap.containsKey(confKey)) {
                        //key already exist
//                        System.out.println("ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                        log.warn(APP_SVN_REV + " " + "ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                    } else {
                        confMap.put(confKey, confValue);
//                        Debug.println("confKey " + confKey, "confValue " + confValue);
//                        System.out.println("confKey " + confKey + " confValue " + confValue);
                        log.trace(APP_SVN_REV + " " + "confKey " + confKey + "confValue " + confValue);
                    }
                }

            }
            //TODO replace petan
            newConfMap = replaceFkPetan(confMap);
            //TODO ADD TPIM
            String tpim_hdfsDir = newConfMap.get("TPIM_PMDATA_HDFS_DIR");
            String tpim_retention = newConfMap.get("TPIM_RETENTION");
            Map<String, String> tpimConfMap = getConfigTpimMap(tpim_hdfsDir, tpim_retention);
//            for (Map.Entry<String, String> entry : newConfMap.entrySet()) {
//                System.out.printf("before putall : %s and Value: %s %n", entry.getKey(), entry.getValue());
//            }
            newConfMap.putAll(tpimConfMap);

            for (Map.Entry<String, String> entry : newConfMap.entrySet()) {
                if (entry.getKey().startsWith(adaptor.toUpperCase())) {
//                    log.debug("Final Key : " + entry.getKey() + " and Value: " + entry.getValue());
                    filteredConfMap.put(entry.getKey(), entry.getValue());
                }
            }

        } catch (FileNotFoundException ex) {
            log.fatal("ERROR: getAdaptorConfigMap FileNotFoundException" + ex);
//            ex.printStackTrace();
        } catch (IOException ex) {
            log.fatal("ERROR: getAdaptorConfigMap IOException" + ex);
        }
        return filteredConfMap;
    }

    /**
     *
     * @param RawHashMap that might contain place holders (<>)
     * @return HasMap with place holders (<>) replaced
     */
    public static Map<String, String> replaceFkPetan(Map<String, String> RawHashMap) {
        Map<String, String> newMap = new HashMap<>();
        for (Map.Entry<String, String> entry : RawHashMap.entrySet()) {
//            System.out.printf("Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
            String StringValue = entry.getValue();

            do {
                Pattern fkPetan = Pattern.compile("(<\\w+>)");
                Matcher m = fkPetan.matcher(StringValue);
                if (m.find()) {
                    String fkPetanNormal = m.group().substring(1, m.group().length() - 1);
//                    System.out.println("found: start:" + m.group() + " normal: " + fkPetanNormal);
                    String fkpval = RawHashMap.get(fkPetanNormal);

                    StringValue = StringValue.replace(m.group(), fkpval);
//                    System.out.println("newval:" + StringValue);
                }
            } while (checkFkPetan(StringValue));
            newMap.put(entry.getKey(), StringValue);
        }
        return newMap;
//        for (Map.Entry<String, String> entry : newMap.entrySet()) {
//            System.out.printf("Final Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
//        }

    }

    /**
     *
     * @param ValueString check for prescence of Petan <>
     * @return boolean true if Petan is found
     */
    public static boolean checkFkPetan(String ValueString) {
        Pattern fkPetan = Pattern.compile("(<\\w+>)");
        Matcher m = fkPetan.matcher(ValueString);

        return m.find();
    }

    /**
     * Returns Hashmap <String, List of <String>>of the config file with the
     * delimiter by defualt "# in the beginning of the file is ignored noparam
     * delimiter is set to "=", TpimconfigFile= tpimConfPath_
     *
     * @param tpim_hdfsDir
     * @param tpim_retention
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static Map<String, String> getConfigTpimMap(String tpim_hdfsDir, String tpim_retention) {
        String configFile = tpimConfPath_;
        String delimiter = "=";
        String commnent = "#";
        log.debug("TPIM String delimiter: " + delimiter + " , TPIM String configFile: " + configFile);
//        System.out.println("TPIM String delimiter: " + delimiter + " , TPIM String configFile: " + configFile);
        Map<String, String> confMap = new HashMap<String, String>();
        Map<String, String> omesConfMap = getOmesTpimMap();
        String confKey = "";
        String confValue = "";
        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {
            //map for fpetans
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains(delimiter) && !line.startsWith(commnent)) {
                    String[] lineSplit = line.split(delimiter);
                    String adaptor = lineSplit[0]; // dapator
                    String[] confValueSplit = lineSplit[1].split(",");
                    //_HDFS_DIR
                    confKey = adaptor.toUpperCase() + "_HDFS_DIR";
                    confValue = tpim_hdfsDir + "/" + adaptor.toLowerCase();
                    confMap.put(confKey, confValue);

                    //_PARTITIONING
                    confKey = adaptor.toUpperCase() + "_PARTITIONING";
                    confValue = confValueSplit[1];
                    confMap.put(confKey, confValue);

                    //_RETENTION
                    confKey = adaptor.toUpperCase() + "_RETENTION";
                    confValue = tpim_retention;
                    confMap.put(confKey, confValue);

                    //_TABLES
                    confKey = adaptor.toUpperCase() + "_TABLES";
                    StringBuilder sb = new StringBuilder();
                    for (int i = 2; i < confValueSplit.length; i++) {
                        String tableName = omesConfMap.get(adaptor.toLowerCase() + "-" + confValueSplit[i].toLowerCase());
                        sb.append(tableName.toLowerCase()).append(",");
                    }
                    confValue = sb.substring(0, sb.length() - 1);
                    confMap.put(confKey, confValue);

                }

            }

        } catch (FileNotFoundException ex) {
            log.fatal(APP_SVN_REV + " " + "FileNotFoundException: " + ex.getMessage());
            System.exit(0);
        } catch (IOException ex) {
            log.fatal(APP_SVN_REV + " " + "IOException: " + ex.getMessage());
            System.exit(0);
        }
        for (Map.Entry<String, String> entry : confMap.entrySet()) {
//            System.out.printf("TPIM Key : %s and Value: %s %n", entry.getKey(), entry.getValue());
            log.trace("TPIM Key : " + entry.getKey() + " and Value: " + entry.getValue());
        }
        return confMap;
    }

    public static Map<String, String> getOmesTpimMap() {
        String configFile = tpimMapFile_;
        String delimiter = ",";
        String commnent = "#";
        log.debug("TPIM String delimiter: " + delimiter + " , TPIM String configFile: " + configFile);
        Map<String, String> confMap = new HashMap<String, String>();
//        Map<String, String> confMap = new HashMap<String, String>();
        String confKey = "";
        String confValue = "";
        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains(delimiter) && !line.startsWith(commnent)) {
                    String[] lineSplit = line.split(delimiter);
                    confKey = lineSplit[3].toLowerCase() + "-" + lineSplit[0].toLowerCase(); // adapter+omes measur
                    confValue = lineSplit[1].toLowerCase();
                    if (confMap.containsKey(confKey)) {
                        //key already exist
                        System.out.println("ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                        log.warn(APP_SVN_REV + " " + "ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                    } else {
                        confMap.put(confKey, confValue);
//                        Debug.println("confKey " + confKey, "confValue " + confValue);
//                        System.out.println("confKey " + confKey + " confValue " + confValue);
                        log.trace(APP_SVN_REV + " " + "confKey " + confKey + "confValue " + confValue);
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            log.fatal(APP_SVN_REV + " " + "FileNotFoundException: " + ex.getMessage());
            System.exit(0);
        } catch (IOException ex) {
            log.fatal(APP_SVN_REV + " " + "IOException: " + ex.getMessage());
            System.exit(0);
        }
        return confMap;
    }

    /**
     * Returns Hashmap <String, List of <String>>of the config file with the
     * delimiter by defualt "# in the beginning of the file is ignored
     *
     * @param delimiter delimiter use in the config file
     * @param valueCount
     * @param configFile full path name of the config file
     * @return Map<String, String> Hasmap of the config file
     * @throws FileNotFoundException
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static Map<String, List<String>> getConfigTpimMap(String delimiter, int valueCount, String configFile) throws FileNotFoundException, IOException, IllegalAccessException {
//        System.out.println("String delimiter: " + delimiter + " , String configFile: " + configFile);
        Map<String, List<String>> confMap = new HashMap<String, List<String>>();
//        Map<String, String> confMap = new HashMap<String, String>();
        String confKey = "";
        String confValue = "";
        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {
            //map for fpetans
            for (String line; (line = br.readLine()) != null;) {
                if (line.contains(delimiter) && !line.startsWith("#")) {

                    String[] lineSplit = line.split(delimiter);
                    confKey = lineSplit[lineSplit.length - 1] + "-" + lineSplit[0];
                    List<String> valueList = new ArrayList<String>();
                    for (int i = 1; i < lineSplit.length - 1; i++) {
                        confValue = lineSplit[i];
                        valueList.add(confValue);
                    }
                    if (confMap.containsKey(confKey)) {
                        //key already exist
//                        System.out.println("ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);
                        log.warn(APP_SVN_REV + " " + "ERROR " + "Duplicate Key: " + confKey + " in config file: \n" + configFile);

                    } else {
                        confMap.put(confKey, valueList);
//                        Debug.println("confKey " + confKey, "confValue " + valueList);
                        log.debug(APP_SVN_REV + " " + "confKey " + confKey + "confValue " + confValue);
//                        System.out.println("confKey " + confKey + " confValue " + confValue);
                    }
                }
            }

        }

        return confMap;
    }

    /**
     *
     * @param Filename
     * @param lineCommentPrefix
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static List<String> readFileToList(String Filename, String lineCommentPrefix) throws FileNotFoundException, IOException {

        log.debug("String lineCommentPrefix: " + lineCommentPrefix + " , String Filename: " + Filename);
        List<String> fileLineList = new ArrayList();
        Pattern pattern = Pattern.compile("^\\s*" + lineCommentPrefix);
        BufferedReader br = new BufferedReader(new FileReader(Filename));
        //map for fpetans
        for (String line; (line = br.readLine()) != null;) {
            Matcher m = pattern.matcher(line);
            if (!m.find()) {
//                System.out.println("matches: " + line);
                fileLineList.add(line);
            } else {
//                System.out.println("skip with prefix " + lineCommentPrefix + " " + line);
//                logIf.UpdateLog(Level.SEVERE, "IOException: " + e);
                log.warn(APP_SVN_REV + " " + "skip with prefix " + lineCommentPrefix + " " + line);
            }
        }

        return fileLineList;
    }

    public static String getHdfsFileName(String hdfsdir, String adaptor, String table, String partition, String pid, String host, String fileDate) {

        String caplogFileName = hdfsdir + "/" + adaptor.toLowerCase() + "_"
                + table.toLowerCase() + "_" + partition.toUpperCase() + "_" + pid + "_" + host + "_" + fileDate + ".csv";
        return caplogFileName;
    }
}
