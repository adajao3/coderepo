/*
 * Copyright 2015 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * <Description>, ArnoldMD 02/02/2015
 *  Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/date/DateUtils.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $ 
 *  Log:    $Log: DateUtils.java,v $
 *  Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 *  Log:    Mulitple updates
 *  Log:   
 */

package com.arnoldmd.hadoop.date;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author adajao2
 */
public class DateUtils {

    /**
     * Get current date and time in specific format
     * @param datePattern e.g. "yyyyMMdd00", "yyyy-MM-dd HH:mm:ss"
     * H	Hour in day (0-23)
     * m	Minute in hour
     * s	Second in minute
     * S	Millisecond
     * d	Day in month
     * M	Month in year
     * y	Year
     * http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
     * @return
     */
    public static String getpdateNow(String datePattern) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
        return dateFormat.format(new Date());

    }
}