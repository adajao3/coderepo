package com.arnoldmd.hadoop.jsch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import javax.swing.ProgressMonitor;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;
import org.apache.log4j.Logger;

public class Jscht {

    private static final String APP_SVN_REV = "$Version: 1.0$".replaceAll("\\$", "").replace("sion", "") + " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
    private static final String APP_NAME = Jscht.class.getSimpleName();
    private static Logger log = Logger.getLogger(APP_NAME);

    private String uname;
    private String passwd;
    private String node;

    public Jscht(String userName, String password, String host) {
        this.uname = userName;
        this.passwd = password;
        this.node = host;
    }

    // SSH Exec to host 1 command only
    /**
     * Run SSH in singe host
     * @param command1
     * @return
     */
    public String SshExec(String command1) {

        String output = "";
        try {

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, 22);
            session.setPassword(passwd);
            session.setConfig(config);
            session.connect();
            // System.out.println("SSH to " + host);

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command1);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            channel.connect();
            byte[] tmp = new byte[5024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 5024);
                    if (i < 0) {
                        break;
                    }
                    output = new String(tmp, 0, i);
                }
                if (channel.isClosed()) {

                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
            return output;
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }

    }

    // SSH Exec to host 1 command only
    /**
     * run SSH and Execute SUDO command
     * @param command1
     * @param sudo_pass
     * @return
     */
    public String SshExecSudo(String command1, String sudo_pass) {

        String output = "";
        try {

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, 22);
            session.setPassword(passwd);
            session.setConfig(config);
            session.connect();
            // System.out.println("SSH to " + host);

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command1);
            channel.setInputStream(null);
            ((ChannelExec) channel).setPty(true);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            OutputStream out = channel.getOutputStream();
            channel.connect();

            out.write((sudo_pass + "\n").getBytes());
            out.flush();
            byte[] tmp = new byte[5024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 5024);
                    if (i < 0) {
                        break;
                    }
                    output = new String(tmp, 0, i);
                }
                if (channel.isClosed()) {

                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
            return output;
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }

    }

    // SSH Exec to host muliplecommands command only
    /**
     * SSH to Multiple Node
     * @param commands
     */
    public void SshShell(List<String> commands) {
        try {
            int sshPort = 22;
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, sshPort);
            session.setPassword(passwd);
            setUpHostKey(session);
            session.connect();

            Channel channel = session.openChannel("shell");// only shell

            channel.setOutputStream(System.out);
            PrintStream shellStream = new PrintStream(channel.getOutputStream()); // printStream
            // for
            // convenience
            // *
			/* InputStream in = channel.getInputStream(); */
            channel.connect();
            for (String command : commands) {
                shellStream.println(command);
                shellStream.flush();
            }

            Thread.sleep(5000);
            channel.connect();

            channel.disconnect();
            session.disconnect();
            System.out.println("done:" + node);
            return;
        } catch (Exception e) {
            System.err.println("ERROR: Connecting via shell to " + node);
            e.printStackTrace();
            return;
            // return null;
        }
    }

    // SCP from server to local file
    /**
     * SCP from remote server to Local file system
     * @param LocalFile
     * @param RemoteFile
     * @return
     */
    public boolean ScpFrom(String LocalFile, String RemoteFile) {
        FileOutputStream fos = null;

        try {

            String prefix = null;
            if (new File(LocalFile).isDirectory()) {
                prefix = LocalFile + File.separator;
            }
            JSch jsch = new JSch();

            Session session = jsch.getSession(uname, node, 22);
            session.setPassword(passwd);
            setUpHostKey(session);
            session.connect();

            String command = "scp -f " + RemoteFile;
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            // get I/O streams for remote scp
            OutputStream out = channel.getOutputStream();
            InputStream in = channel.getInputStream();
            channel.connect();

            byte[] buf = new byte[1024];
            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();

            while (true) {
                int c = checkAck(in);
                if (c != 'C') {
                    break;
                }

                // read '0644 '
                in.read(buf, 0, 5);

                long filesize = 0L;
                while (true) {
                    if (in.read(buf, 0, 1) < 0) {
                        // error
                        break;
                    }
                    if (buf[0] == ' ') {
                        break;
                    }
                    filesize = filesize * 10L + (long) (buf[0] - '0');
                }

                String file = null;
                for (int i = 0;; i++) {
                    in.read(buf, i, 1);
                    if (buf[i] == (byte) 0x0a) {
                        file = new String(buf, 0, i);
                        break;
                    }
                }

                // send '\0'
                buf[0] = 0;
                out.write(buf, 0, 1);
                out.flush();

                // read a content of lfile
                fos = new FileOutputStream(prefix == null ? LocalFile : prefix
                        + file);
                int foo;
                while (true) {
                    if (buf.length < filesize) {
                        foo = buf.length;
                    } else {
                        foo = (int) filesize;
                    }
                    foo = in.read(buf, 0, foo);
                    if (foo < 0) {
                        // error
                        // return false;
                        break;

                    }
                    fos.write(buf, 0, foo);
                    filesize -= foo;
                    if (filesize == 0L) {
                        break;
                    }
                    // return false;

                }
                fos.close();
                fos = null;

                if (checkAck(in) != 0) {
                    return false;
                    // System.exit(0);
                }

                // send '\0'
                buf[0] = 0;
                out.write(buf, 0, 1);
                out.flush();
            }

            session.disconnect();
            return true;
            // System.exit(0);
        } catch (Exception e) {
            System.out.println(e);
            try {
                if (fos != null) {
                    fos.close();
                }
                return false;
            } catch (Exception ee) {
                return false;
            }
        }
    }

    // SCP from local file to server
    /**
     * Send file from Local FS to Remote server
     * @param lfile
     * @param RemoteFile
     * @return
     */
    public boolean ScpTo(String lfile, String RemoteFile) {

        FileInputStream fis = null;
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, 22);
            session.setPassword(passwd);
            setUpHostKey(session);
            session.connect();

            String command = "scp " + " -t " + RemoteFile;
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            // get I/O streams for remote scp
            OutputStream out = channel.getOutputStream();
            InputStream in = channel.getInputStream();

            channel.connect();

            if (checkAck(in) != 0) {
                return false;
                // System.exit(0);

            }

            File _lfile = new File(lfile);

            // send "C0644 filesize filename", where filename should not include
            // '/'
            long filesize = _lfile.length();
            command = "C0644 " + filesize + " ";
            if (lfile.lastIndexOf('/') > 0) {
                command += lfile.substring(lfile.lastIndexOf('/') + 1);
            } else {
                command += lfile;
            }
            command += "\n";
            out.write(command.getBytes());
            out.flush();
            if (checkAck(in) != 0) {
                return false;
                // System.exit(0);
            }

            // send a content of lfile
            fis = new FileInputStream(lfile);
            byte[] buf = new byte[1024];
            while (true) {
                int len = fis.read(buf, 0, buf.length);
                if (len <= 0) {
                    break;
                }
                out.write(buf, 0, len); // out.flush();
            }
            fis.close();
            fis = null;
            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();
            if (checkAck(in) != 0) {
                return false;
                // System.exit(0);
            }
            out.close();

            channel.disconnect();
            session.disconnect();
            return true;
            // System.exit(0);

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

    }

    /**
     * Copy remote server file
     * @param LocalFile
     * @param RemoteFile
     */
    public void SftpGet(String LocalFile, String RemoteFile) {

        try {
//            SimpleDateFormat fm = new SimpleDateFormat("yyyyMMdd");
            int sftpPort = 22;
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, sftpPort);
            session.setPassword(passwd);
            setUpHostKey(session);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp chSftp = (ChannelSftp) channel;
            SftpProgressMonitor monitor = new MyProgressMonitor();
            int mode = ChannelSftp.OVERWRITE;

            chSftp.get(RemoteFile, LocalFile, monitor, mode);

            System.out.println("ftp Completed");
        } catch (Exception e) {
            System.out.println(e);

        }

    }

    /**
     * Send file to remote server
     * @param LocalFile
     * @param RemoteFile
     */
        public void SftpPut(String LocalFile, String RemoteFile) {

        try {
//            SimpleDateFormat fm = new SimpleDateFormat("yyyyMMdd");
            int sftpPort = 22;
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, sftpPort);
            session.setPassword(passwd);
            setUpHostKey(session);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp chSftp = (ChannelSftp) channel;
            SftpProgressMonitor monitor = new MyProgressMonitor();
            int mode = ChannelSftp.OVERWRITE;

            chSftp.put(LocalFile, RemoteFile, monitor, mode);

            System.out.println("ftp Completed");
        } catch (Exception e) {
            System.out.println(e);

        }

    }
    public void SftpFolder(String LocalFolder, String RemoteFolder) {

        try {
//            SimpleDateFormat fm = new SimpleDateFormat("yyyyMMdd");
            int sftpPort = 22;
            JSch jsch = new JSch();
            Session session = jsch.getSession(uname, node, sftpPort);
            session.setPassword(passwd);
            setUpHostKey(session);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp chSftp = (ChannelSftp) channel;
//            SftpProgressMonitor monitor = new MyProgressMonitor();
            int mode = ChannelSftp.OVERWRITE;
            try {
                java.util.Vector vv = chSftp.ls(RemoteFolder);
                if (vv != null) {
                    for (int ii = 0; ii < vv.size(); ii++) {


                        Object obj = vv.elementAt(ii);
                        if (obj instanceof com.jcraft.jsch.ChannelSftp.LsEntry) {
                            System.out.println(((com.jcraft.jsch.ChannelSftp.LsEntry) obj).getLongname());
                        }

                    }
                }
            } catch (SftpException e) {
                System.out.println(e.toString());
            }

            System.out.println("ftp Completed");
        } catch (Exception e) {
            System.out.println(e);

        }

    }

    public static class MyProgressMonitor implements SftpProgressMonitor {

        ProgressMonitor monitor;
        long count = 0;
        long max = 0;

        public void init(int op, String src, String dest, long max) {
            this.max = max;
            monitor = new ProgressMonitor(null,
                    ((op == SftpProgressMonitor.PUT) ? "put" : "get") + ": "
                    + src, "", 0, (int) max);
            count = 0;
            percent = -1;
            monitor.setProgress((int) this.count);
            monitor.setMillisToDecideToPopup(1000);
        }

        private long percent = -1;

        public boolean count(long count) {
            this.count += count;

            if (percent >= this.count * 100 / max) {
                return true;
            }
            percent = this.count * 100 / max;

            monitor.setNote("Completed " + this.count + "(" + percent
                    + "%) out of " + max + ".");
            monitor.setProgress((int) this.count);

            return !(monitor.isCanceled());
        }

        public void end() {
            monitor.close();
        }
    }

    static int checkAck(InputStream in) throws IOException {
        int b = in.read();
        // b may be 0 for success,
        // 1 for error,
        // 2 for fatal error,
        // -1
        if (b == 0) {
            return b;
        }
        if (b == -1) {
            return b;
        }

        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c;
            do {
                c = in.read();
                sb.append((char) c);
            } while (c != '\n');
            if (b == 1) { // error
                System.out.print(sb.toString());
            }
            if (b == 2) { // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }

    public static void setUpHostKey(Session session) {
        // Note: There are two options to connect
        // 1: Set StrictHostKeyChecking to no
        // Create a Properties Object
        // Set StrictHostKeyChecking to no
        // session.setConfig(config);
        // 2: Use the KnownHosts File
        // Manually ssh into the appropriate machines via unix
        // Go into the .ssh\known_hosts file and grab the entries for the hosts
        // Add the entries to a known_hosts file
        // jsch.setKnownHosts(khfile);
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
    }

    public static String sendCurl(String zdLiveTableName, String zdLiveJsonData) {
        String host = "10.46.50.105";
        String user = "omc";
        String passwd = "1r0ncity";
        String cmd = "curl -v --user admin:admin1 https://10.46.58.86:8443/zoomdata/service/upload?source=" + zdLiveTableName + " -X POST -H 'Content-Type: application/json' -d '" + zdLiveJsonData + "' --insecure";
        Jscht jscht = new Jscht(user, passwd, host);
        log.debug("curl:" + cmd);
        String result = jscht.SshExec(cmd);

        return result;
    }

}
