/*
 * Copyright 2015 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 03/02/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/utils/RandomInteger.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: RandomInteger.java,v $
 * <Description>, ArnoldMD 03/02/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 03/02/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 03/02/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.utils;

import java.util.Random;
import org.apache.log4j.Logger;

public class RandomInteger {

    private static final String APP_SVN_REV = "$Version: 1.0$".replaceAll("\\$", "").replace("sion", "") + " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
    private static final String APP_NAME = RandomInteger.class.getSimpleName();
    private static Logger log = Logger.getLogger(APP_NAME);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    public static int RandomInt(int min, int max) {
        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
