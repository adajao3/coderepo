/*
 * Copyright 2014 ArnoldMD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * <Description>, ArnoldMD 11/24/2014
 *  Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/logging/LogFormatter.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $ 
 *  Log:    $Log: LogFormatter.java,v $
 *  Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 *  Log:    Mulitple updates
 *  Log:   
 *  Log:    Revision 1.2  2014/11/25 02:22:47  arnoldmd
 *  Log:    Update for HDFS utilities amd merging
 *  Log:   
 */
package com.arnoldmd.hadoop.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * 
 * @author adajao2
 */
public class LogFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String level = record.getLevel().toString().matches("SEVERE")? "ERROR" : record.getLevel().toString();
        return sdf.format(new Date(record.getMillis())) + " [" + record.getThreadID() + "] " + level + " "+ record.getSourceClassName() + " "
                + record.getSourceMethodName() + " "
                + record.getMessage() + "\n";
    }

}
