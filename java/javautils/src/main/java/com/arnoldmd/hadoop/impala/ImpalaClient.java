/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: ImpalaClient.java,v $
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Mulitple updates
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    Revision 1.2  2014/12/09 18:25:49  arnoldmd
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    updated Mail and HDFS
 * <Description>, ArnoldMD 11/14/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/impala/ImpalaClient.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:   
 */
package com.arnoldmd.hadoop.impala;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;

/**
 *
 * @author adajao2
 */
public class ImpalaClient {
	private  final String APP_NAME = ImpalaClient.class.getSimpleName();
	private  Logger log = Logger.getLogger(APP_NAME);

	/**
     *
     */
	public ImpalaClient() {
		String driverName = "org.apache.hive.jdbc.HiveDriver";
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * Execute quesy with resultset as return
	 *
	 * @param sqlCommand
	 * @return ResultSet
	 * @throws SQLException
	 */
	public ResultSet getResultSet(String sqlCommand) throws SQLException {
		// replace "hive" here with the name of the user the queries should run
		// as
		String jdbcUrl = "jdbc:hive2://10.46.50.254:25004/preproduction;auth=noSasl";
		Connection con = DriverManager.getConnection(jdbcUrl);
		Statement stmt = con.createStatement();
		if (sqlCommand.matches(".*;")) {
			sqlCommand = sqlCommand.replaceAll(";", "");
		}
		// System.out.println("Running: " + sqlCommand);
		log.debug("Running: " + sqlCommand);
		ResultSet res = stmt.executeQuery(sqlCommand);
		if (res.next()) {
			// System.out.println(res.getString(1));
			log.debug("Running: " + res.getString(1));
		}
		return res;
	}

	/**
	 * Execute query without any return
	 *
	 * @param sqlCommand
	 * @return boolean
	 */
	public boolean execQuery(String sqlCommand) {
		// replace "hive" here with the name of the user the queries should run
		// as
		boolean executed = false;
		try {
			String jdbcUrl = "jdbc:hive2://10.46.50.254:25004/preproduction;auth=noSasl";
			Connection con = DriverManager.getConnection(jdbcUrl);
			Statement stmt = con.createStatement();
			System.out.println("Running: " + sqlCommand);
			if (sqlCommand.matches(".*;")) {
				sqlCommand = sqlCommand.replaceAll(";", "");
			}
			executed = stmt.execute(sqlCommand);
			executed = true;
		} catch (SQLException ex) {
			// Logger.getLogger(ImpalaClient.class.getName()).log(Level.SEVERE,
			// "SQLException", ex);
			return executed;
		}
		return executed;
	}

	
	public  void workAroound() throws IOException {
		// start work around//
		File workaround = new File(".");
		System.getProperties().put("hadoop.home.dir",
				workaround.getAbsolutePath());
		new File("./bin").mkdirs();
		new File("./bin/winutils.exe").createNewFile();
		// * end work around//
	}

}
