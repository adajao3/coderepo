/*
 * Copyright 2014 ArnoldMD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * <Description>, ArnoldMD 11/24/2014
 *  Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/utils/Adaptor.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $ 
 *  Log:    $Log: Adaptor.java,v $
 *  Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 *  Log:    Mulitple updates
 *  Log:   
 *  Log:    Revision 1.1  2014/12/09 18:25:49  arnoldmd
 *  Log:    updated Mail and HDFS
 *  Log:   
 *  Log:    Revision 1.2  2014/11/25 02:22:47  arnoldmd
 *  Log:    Update for HDFS utilities amd merging
 *  Log:   
 */
package com.arnoldmd.hadoop.utils;

/**
 *
 * @author adajao2
 */
public class Adaptor{

    //fields
    private String tables_;
    private String partitioning_;
    private String importPath_;
    private int trace_;
    private String name_;
    private String rawSql_;
    private int columns_;
    private int retention_;
    private String hdfsPath_;
    private int header_;

    //constructor

    /**
     * Creates adaptor with all parameters 
     * @param tables
     * @param partitioning
     * @param importPath
     * @param trace
     * @param name
     * @param rawSql
     * @param columns
     * @param retention
     * @param hdfsPath
     * @param header
     */
        public void CreateAdaptor(String tables, String partitioning, String importPath, int trace, String name, String rawSql, int columns, int retention, String hdfsPath, int header){
        tables_ = tables;
        partitioning_ = partitioning;
        importPath_ = importPath;
        trace_ = trace;
        name_ = name;
        rawSql_ = rawSql;
        columns_ = columns;
        retention_ = retention;
        hdfsPath_ = hdfsPath;
        header_ = header;

    }

    //methods
    //setter

    /**
     *
     * @param newTables
     */
        public void setTables (String newTables){
        tables_ = newTables;
    }

    /**
     *
     * @param newPartitioning
     */
    public void setPartitioning (String newPartitioning){
        partitioning_ = newPartitioning;
    }

    /**
     *
     * @param newImportPath
     */
    public void setImportPath (String newImportPath){
        importPath_ = newImportPath;
    }

    /**
     *
     * @param newTrace
     */
    public void setTrace (int newTrace){
        trace_ = newTrace;
    }

    /**
     *
     * @param newName
     */
    public void setName (String newName){
        name_ = newName;
    }

    /**
     *
     * @param newRawSql
     */
    public void setRawSql (String newRawSql){
        rawSql_ = newRawSql;
    }

    /**
     *
     * @param newColumns
     */
    public void setColumns (int newColumns){
        columns_ = newColumns;
    }

    /**
     *
     * @param newRetention
     */
    public void setRetention (int newRetention){
        retention_ = newRetention;
    }

    /**
     *
     * @param newHdfsPath
     */
    public void setHdfsPath (String newHdfsPath){
        hdfsPath_ = newHdfsPath;
    }

    /**
     *
     * @param newHeader
     */
    public void setHeader (int newHeader){
        header_ = newHeader;
    }


    //getter

    /**
     *
     * @return Tables
     */
        public String getTables (){
        return tables_;
    }

    /**
     *
     * @return Partitioning
     */
    public String getPartitioning (){
        return partitioning_;
    }

    /**
     *
     * @return getImportPath
     */
    public String getImportPath (){
        return importPath_;
    }

    /**
     *
     * @return Trace
     */
    public int getTrace (){
        return trace_;
    }

    /**
     *
     * @return Name
     */
    public String getName (){
        return name_;
    }

    /**
     *
     * @return RawSql
     */
    public String getRawSql (){
        return rawSql_;
    }

    /**
     *
     * @return Columns
     */
    public int getColumns (){
        return columns_;
    }

    /**
     *
     * @return Retention
     */ 
    public int getRetention (){
        return retention_;
    }

    /**
     *
     * @return HdfsPath
     */
    public String getHdfsPath (){
        return hdfsPath_;
    }

    /**
     *
     * @return Header
     */
    public int getHeader (){
        return header_;
    }


}

