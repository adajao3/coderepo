/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * <Description>, ArnoldMD 02/25/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/zoomdata/TpimAlarm.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: TpimAlarm.java,v $
 * <Description>, ArnoldMD 02/25/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 02/25/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 02/25/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.zoomdata;

public class TpimAlarm {

    //fields
    private String cronRunTime;
    private String updateTime;
    private String lastRun;
    private int pid;
    private String log;
    private String ip;
    private String appName;
    private String alarm;
    private String node;
    private int duration;
    private int warningAlarm;
    private String description;
    private String errorDetails;
    private int criticalAlarm;
    private String user;
    private int running;

    //constructor
    public TpimAlarm(String cronRunTime, String updateTime, String lastRun, int pid, String log, String ip, String appName, String alarm, String node, int duration, int warningAlarm, String description, String errorDetails, int criticalAlarm, String user, int running) {
        this.cronRunTime = cronRunTime;
        this.updateTime = updateTime;
        this.lastRun = lastRun;
        this.pid = pid;
        this.log = log;
        this.ip = ip;
        this.appName = appName;
        this.alarm = alarm;
        this.node = node;
        this.duration = duration;
        this.warningAlarm = warningAlarm;
        this.description = description;
        this.errorDetails = errorDetails;
        this.criticalAlarm = criticalAlarm;
        this.user = user;
        this.running = running;

    }

    TpimAlarm() {

    }

    //methods
    //setter
    public void setCronRunTime(String newCronRunTime) {
        this.cronRunTime = newCronRunTime;
    }

    public void setUpdateTime(String newUpdateTime) {
        this.updateTime = newUpdateTime;
    }

    public void setLastRun(String newLastRun) {
        this.lastRun = newLastRun;
    }

    public void setPid(int newPid) {
        this.pid = newPid;
    }

    public void setLog(String newLog) {
        this.log = newLog;
    }

    public void setIp(String newIp) {
        this.ip = newIp;
    }

    public void setAppName(String newAppName) {
        this.appName = newAppName;
    }

    public void setAlarm(String newAlarm) {
        this.alarm = newAlarm;
    }

    public void setNode(String newNode) {
        this.node = newNode;
    }

    public void setDuration(int newDuration) {
        this.duration = newDuration;
    }

    public void setWarningAlarm(int newWarningAlarm) {
        this.warningAlarm = newWarningAlarm;
    }

    public void setDescription(String newDescription) {
        this.description = newDescription;
    }

    public void setErrorList(String newErrorList) {
        this.errorDetails = newErrorList;
    }

    public void setCriticalAlarm(int newCriticalAlarm) {
        this.criticalAlarm = newCriticalAlarm;
    }

    public void setUser(String newUser) {
        this.user = newUser;
    }

  
    public void setRunning(int newRunning) {
        this.running = newRunning;
    }

    //getter
    public String getCronRunTime() {
        return this.cronRunTime;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public String getLastRun() {
        return this.lastRun;
    }

    public int getPid() {
        return this.pid;
    }

    public String getLog() {
        return this.log;
    }

    public String getIp() {
        return this.ip;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getAlarm() {
        return this.alarm;
    }

    public String getNode() {
        return this.node;
    }

    public int getDuration() {
        return this.duration;
    }

    public int getWarningAlarm() {
        return this.warningAlarm;
    }

    public String getDescription() {
        return this.description;
    }

    public String getErrorList() {
        return this.errorDetails;
    }

    public int getCriticalAlarm() {
        return this.criticalAlarm;
    }

    public String getUser() {
        return this.user;
    }


    public int getRunning() {
        return this.running;
    }
}
