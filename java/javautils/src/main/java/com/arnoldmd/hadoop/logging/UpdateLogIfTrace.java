/*
 * Copyright 2014 ArnoldMD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * <Description>, ArnoldMD 11/24/2014
 *  Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/logging/UpdateLogIfTrace.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $ 
 *  Log:    $Log: UpdateLogIfTrace.java,v $
 *  Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 *  Log:    Mulitple updates
 *  Log:   
 *  Log:    Revision 1.3  2014/12/09 18:25:49  arnoldmd
 *  Log:    updated Mail and HDFS
 *  Log:   
 *  Log:    Revision 1.2  2014/11/25 02:22:47  arnoldmd
 *  Log:    Update for HDFS utilities amd merging
 *  Log:   
 */
package com.arnoldmd.hadoop.logging;

import com.arnoldmd.hadoop.utils.getMainClass;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 *
 * @author adajao2
 */
public class UpdateLogIfTrace {

	private static Logger logger = Logger.getLogger(UpdateLogIfTrace.class
			.getName());
	private static boolean noexeptions;

	/**
	 * Initialize log file logMessage is the message written on the log file
	 *
	 * @param LogFileName
	 *            Full pathname of the logfile
	 * @param LoggingProperties
	 *            LoggingProperties file
	 */
	public UpdateLogIfTrace(String LogFileName, String LoggingProperties) {
		try {
			LogManager.getLogManager().readConfiguration(
					new FileInputStream(LoggingProperties));
		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, " Exception " + e1.getMessage());
			noexeptions = false;
			return;
		}

		logger.setLevel(Level.FINE);
		logger.addHandler(new ConsoleHandler());
		// adding custom handler
		logger.addHandler(new LogHandler());
		try {
			// FileHandler file name with max size and number of log files limit
			// Handler fileHandler = new FileHandler("C:\\Temp\\logger.log",
			// 2000, 5);
			Handler fileHandler = new FileHandler(LogFileName);
			fileHandler.setFormatter(new LogFormatter());
			// setting custom filter for FileHandler
			// fileHandler.setFilter(new MyFilter());
			logger.addHandler(fileHandler);
			logger.log(Level.INFO, "Starting App");
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, " Exception " + e.getMessage());
			noexeptions = false;
			return;
		}
		noexeptions = true;
	}

	/**
	 *
	 * @param LoggingProperties
	 */
	public UpdateLogIfTrace(String LoggingProperties) {
		String logFilePath = System.getenv("OMCLOGDIR");
		String hostName = System.getenv("HOSTNAME");
		String appName = getMainClass.getMainClassNMame();
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String LogFileName = logFilePath + "/" + appName + "_" + hostName + "_"
				+ date + ".log";

		System.out.println("LogFileName: " + LogFileName);
		try {
			LogManager.getLogManager().readConfiguration(
					new FileInputStream(LoggingProperties));
		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, " Exception " + e1.getMessage());
			noexeptions = false;
			return;
		}

		logger.setLevel(Level.FINE);
		logger.addHandler(new ConsoleHandler());
		// adding custom handler
		logger.addHandler(new LogHandler());
		try {
			// FileHandler file name with max size and number of log files limit
			// Handler fileHandler = new FileHandler("C:\\Temp\\logger.log",
			// 2000, 5);
			/**
			 * Define whether the log should be appended to the existing one If
			 * set to false, the log file will be overwritten, not appended
			 */
			boolean append = true;

			/**
			 * Defining the limit of file size in Bytes. This is tentatively the
			 * maximum data that will be written to one log file.
			 */
			int limit = 1000024; // 1 GB Per log file, modify as required.

			/**
			 * If a file is full (as defined by 'limit' value above), files will
			 * be renamed and a new log file will be crated.
			 *
			 * Pattern defines how the log file should be renamed.
			 *
			 * In this example, we are using the same fileName as the pattern.
			 * So the files will be renamed as: myAppLogFile.log.0
			 * myAppLogFile.log.1 myAppLogFile.log.2 ..and so on.
			 */
			/**
			 * This is the maximum number of log files to be created. If this
			 * number is reached, the last file will be truncated and a new one
			 * will be created.
			 */
			int numLogFiles = 1000;

			Handler fileHandler = new FileHandler(LogFileName, limit,
					numLogFiles, append);
			fileHandler.setFormatter(new LogFormatter());
			// setting custom filter for FileHandler
			// fileHandler.setFilter(new MyFilter());
			logger.addHandler(fileHandler);
			logger.log(Level.INFO, "Starting App");
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, " Exception " + e.getMessage());
			noexeptions = false;
			return;
		}
		noexeptions = true;
	}

	/**
	 *
	 * @return boolean
	 */
	public static boolean checkExeptions() {
		return noexeptions;
	}

	/**
	 * update the log file with specified logLevel for "ERROR" use Level.SEVERE
	 * logMessage is the message written on the log file
	 *
	 * @param logLevel
	 * @param logMesssage
	 */
	public static void UpdateLog(Level logLevel, String logMesssage) {
		logger.log(logLevel, logMesssage);

	}

}
