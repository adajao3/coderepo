/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 12/12/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/logging/Log4jTpim.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: Log4jTpim.java,v $
 * <Description>, ArnoldMD 12/12/2014   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 12/12/2014   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 12/12/2014   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.logging;

import com.arnoldmd.hadoop.utils.getMainClass;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author adajao2
 */
public class Log4jAppUtils {

    private static final String APP_SVN_REV = "$Version: 2.0$".replaceAll("\\$", "").replace("sion", "") + " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
    private static final String APP_NAME = Log4jAppUtils.class.getSimpleName();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    /**
     *
     * @param LogFileName full path of log file
     * @param LoggingThreshold Logging level
     * @ ALL: The ALL has the lowest possible rank and is intended to turn on
     * all logging.
     * @ DEBUG: The DEBUG Level designates fine-grained informational events
     * that are most useful to debug an application.
     * @ ERROR: The ERROR level designates error events that might still allow
     * the application to continue running.
     * @ FATAL: The FATAL level designates very severe error events that will
     * presumably lead the application to abort.
     * @ INFO: The INFO level designates informational messages that highlight
     * the progress of the application at coarse-grained level.
     * @ OFF: The OFF has the highest possible rank and is intended to turn off
     * logging.
     * @ TRACE: The TRACE Level designates finer-grained informational events
     * than the DEBUG
     * @ WARN: The WARN level designates potentially harmful situations.
     */
    public void setlog4j1Properties(String LogFileName, Level LoggingThreshold) {
        MDC.put("PID", getPID());
        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile(LogFileName);
        fa.setLayout(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %X{PID} %5p [%t] (%F:%L) - %m%n"));
        fa.setThreshold(LoggingThreshold);
        fa.setAppend(true);
        fa.activateOptions();
        Logger.getRootLogger().addAppender(fa);

    }

    /**
     *
     * @param LoggingThreshold
     */
    public void setlog4j1Properties(Level LoggingThreshold) {
        MDC.put("PID", getPID());
        String logFilePath = System.getenv("OMCLOGDIR");
        String hostName = System.getenv("HOSTNAME");
        String appName = getMainClass.getMainClassShortName();
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String LogFileName = logFilePath + "/" + appName + "_" + hostName + "_" + date + ".log";

        System.out.println("LogFileName: " + LogFileName);
        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile(LogFileName);
        fa.setLayout(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %X{PID} %5p [%t] (%F:%L) - %m%n"));
        fa.setThreshold(LoggingThreshold);
        fa.setAppend(true);
        fa.activateOptions();
        Logger.getRootLogger().addAppender(fa);

    }

    /**
     * Set Console output threshold
     * @param LoggingThreshold = use org.apache.log4j.Level
     */
    public void setConsoleLogThreshold(Level LoggingThreshold) {
        try {
            Properties props = new Properties();

            props.load(new InputStreamReader(Thread.currentThread().
                    getContextClassLoader().getResourceAsStream("log4j.properties")));
            props.setProperty("log4j.appender.stdout.Threshold", LoggingThreshold.toString());
            PropertyConfigurator.configure(props);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Log4jAppUtils.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }

    private static String getPID() {
        RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
        return rt.getName();
    }

}
