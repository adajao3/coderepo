/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com//hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    $Log: HdfsContent.java,v $
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Mulitple updates
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.2 2014/12/09
 * 18:25:49 arnoldmd
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    updated Mail and HDFS
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1 2014/11/25
 * 02:22:47 arnoldmd
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Update for HDFS utilities
 * amd merging
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsContent.java,v
 * 1.2 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 */
package com.arnoldmd.hadoop.hdfs;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author adajao2
 */
public class HdfsContent {

	private final String APP_NAME = HdfsContent.class.getSimpleName();
	Logger log = Logger.getLogger(APP_NAME);

	// fields
	private int PathCount_;
	private int FileCount_;
	private int ErrorCount_;
	private List<String> Files_;
	private List<String> Errors_;
	private List<String> Paths_;
	private Map<String, Integer> PathFileCount_;
	private Map<String, String> PathOldestFile_;
	private Map<String, Long> PathFileSize_;

	// constructor
	/**
	 *
	 * @param PathCount
	 * @param Files
	 * @param ErrorCount
	 * @param Paths
	 * @param FileCount
	 * @param Errors
	 * @param PathFileCount
	 * @param PathOldestFile
	 */
	public void NewHdfsContent(int PathCount, int FileCount, int ErrorCount,
			List<String> Files, List<String> Paths, List<String> Errors,
			Map<String, Integer> PathFileCount,
			Map<String, String> PathOldestFile) {
		ErrorCount_ = ErrorCount;
		PathCount_ = PathCount;
		Files_ = Files;
		Paths_ = Paths;
		Errors_ = Errors;
		FileCount_ = FileCount;
		PathFileCount_ = PathFileCount;
		PathOldestFile_ = PathOldestFile;
	}

	// constructor
	/**
	 *
	 * @param PathCount
	 * @param Files
	 * @param ErrorCount
	 * @param Paths
	 * @param FileCount
	 * @param Errors
	 * @param PathFileCount
	 * @param PathOldestFile
	 * @param PathFileSize
	 */
	public void NewHdfsContent(int PathCount, int FileCount, int ErrorCount,
			List<String> Files, List<String> Paths, List<String> Errors,
			Map<String, Integer> PathFileCount,
			Map<String, String> PathOldestFile, Map<String, Long> PathFileSize) {
		ErrorCount_ = ErrorCount;
		PathCount_ = PathCount;
		Files_ = Files;
		Paths_ = Paths;
		Errors_ = Errors;
		FileCount_ = FileCount;
		PathFileCount_ = PathFileCount;
		PathOldestFile_ = PathOldestFile;
		PathFileSize_ = PathFileSize;
	}

	// methods
	// setter
	/**
	 *
	 * @param newErrorCount
	 */
	public void setErrorCount(int newErrorCount) {
		ErrorCount_ = newErrorCount;
	}

	/**
	 *
	 * @param newPathCount
	 */
	public void setPathCount(int newPathCount) {
		PathCount_ = newPathCount;
	}

	/**
	 *
	 * @param newFiles
	 */
	public void setFiles(List<String> newFiles) {
		Files_ = newFiles;
	}

	/**
	 *
	 * @param newPaths
	 */
	public void setPaths(List<String> newPaths) {
		Paths_ = newPaths;
	}

	/**
	 *
	 * @param newFileCount
	 */
	public void setFileCount(int newFileCount) {
		FileCount_ = newFileCount;
	}

	/**
	 *
	 * @param newErrors
	 */
	public void setErrors(List<String> newErrors) {
		Errors_ = newErrors;
	}

	// getter
	/**
	 *
	 * @return
	 */
	public int getErrorCount() {
		return ErrorCount_;
	}

	/**
	 *
	 * @return Paths in HDFS
	 */
	public int getPathCount() {
		return PathCount_;
	}

	/**
	 *
	 * @return Files in HDFS
	 */
	public List<String> getFiles() {
		return Files_;
	}

	/**
	 *
	 * @return
	 */
	public List<String> getPaths() {
		return Paths_;
	}

	/**
	 *
	 * @return
	 */
	public int getFileCount() {
		return FileCount_;
	}

	/**
	 *
	 * @return
	 */
	public List<String> getErrors() {
		return Errors_;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, Integer> getPathFileCount() {
		return PathFileCount_;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, Long> getPathFileSize() {
		return PathFileSize_;
	}

	/**
	 *
	 * @return
	 */
	public Long getPathSize(String Path) {
		if (PathFileSize_.get(Path) == null) {
			return 0L;
		} else {
			return PathFileSize_.get(Path);
		}

	}

	/**
	 *
	 * @return
	 */
	public Integer getPathFileCount(String Path) {

		if (PathFileCount_.get(Path) == null) {
			return 0;
		} else {
			return PathFileCount_.get(Path);
		}
	}

	/**
	 *
	 * @param FileCountFilter
	 *            minimum file count to in directory
	 * @return PathFileCountFiltered
	 */
	public Map<String, Integer> getPathFileCount(int FileCountFilter) {
		Map<String, Integer> PathFileCountFiltered_ = new HashMap<String, Integer>();
		Iterator<Map.Entry<String, Integer>> iter = PathFileCount_.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Map.Entry<String, Integer> entry = iter.next();
			// System.out.println("FileCountFilter:" + FileCountFilter + " Key:"
			// + entry.getKey() + " Value:" + entry.getValue());
			log.debug("FileCountFilter:" + FileCountFilter + " Key:"
					+ entry.getKey() + " Value:" + entry.getValue());

			if (entry.getValue() >= FileCountFilter) {
				PathFileCountFiltered_.put(entry.getKey(), entry.getValue());
				System.out.println("ADD FileCountFilter:" + FileCountFilter
						+ " Key:" + entry.getKey() + " Value:"
						+ entry.getValue());
				log.debug("ADD FileCountFilter:" + FileCountFilter + " Key:"
						+ entry.getKey() + " Value:" + entry.getValue());
			}
		}
		return PathFileCountFiltered_;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, String> getPathOldestFile() {
		return PathOldestFile_;
	}

	public String getPartitionTable(String hdfsPath) {
		// check if partition
		String tableName = new File(hdfsPath).getName();
		Pattern pattern = Pattern.compile("(.*pdate=)[A-Za-z0-9]");
		Matcher m = pattern.matcher(hdfsPath.toLowerCase());
		if (m.find()) {

			tableName = new File(m.group(1)).getParentFile().getName();
			// System.out.println("match");
		} else {
			// csv
			pattern = Pattern.compile("(.*)/.*.csv$");
			m = pattern.matcher(hdfsPath.toLowerCase());
			if (m.find()) {
				tableName = new File(m.group(1)).getName();
			}

		}
		return tableName;
	}

	public String getPartitionAdaptor(String hdfsPath) {
		// check if partition
		String adaptorName = new File(hdfsPath).getParentFile().getName();
		Pattern pattern = Pattern.compile("(.*pdate=)[A-Za-z0-9]");
		Matcher m = pattern.matcher(hdfsPath.toLowerCase());
		if (m.find()) {

			adaptorName = new File(m.group(1)).getParentFile().getParentFile()
					.getName();

			// System.out.println("match");
		} else {
			// csv
			pattern = Pattern.compile("(.*)/.*.csv$");
			m = pattern.matcher(hdfsPath.toLowerCase());
			if (m.find()) {
				adaptorName = new File(m.group(1)).getParentFile().getName();
			}

		}
		return adaptorName;
	}

	public String getPartitionFolder(String hdfsPath) {
		// check if partition
		String partionName = "";
		Pattern pattern = Pattern.compile("(.*pdate=)([A-Za-z0-9]*)/");
		Matcher m = pattern.matcher(hdfsPath);
		if (m.find()) {

			partionName = m.group(2);
			// System.out.println("match");
		} else {
			// csv
			pattern = Pattern.compile("(.*pdate=)([A-Za-z0-9]*)");
			m = pattern.matcher(hdfsPath);
			if (m.find()) {
				partionName = m.group(2);
			}
		}
		return partionName;
	}

}
