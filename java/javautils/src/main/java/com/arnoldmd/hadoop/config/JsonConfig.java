/*
 * Copyright 2015 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 02/19/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/config/JsonConfig.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: JsonConfig.java,v $
 * <Description>, ArnoldMD 02/19/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 02/19/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 02/19/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.config;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * Read Application configuration in JSON
 * 
 * @author adajao2
 *
 */
public class JsonConfig {

	private static final String APP_NAME = JsonConfig.class.getSimpleName();
	private static final Logger log = Logger.getLogger(APP_NAME);

	/**
	 * Parse JSON file to Application Configuration
	 * @param JsonConfFile full path name of JSON file
	 * @return Application configuration "AppUtilsConfig" object
	 * @exception catch IO exception to logs
	 */
	public AppUtilsConfig getJsonConfig(String JsonConfFile) {
		try {
			Gson gson = new Gson();
			log.debug("Reading Json Config File:" + JsonConfFile);
			BufferedReader br = new BufferedReader(new FileReader(JsonConfFile));

			// convert the JSON string back to object
			return (gson.fromJson(br, AppUtilsConfig.class));

		} catch (IOException e) {
			log.fatal("IOException reading Json Config File:" + JsonConfFile
					+ " " + e);
			return null;
		}

	}

}
