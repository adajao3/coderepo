/*
 *  Loghandler for saving logs, arnoldmd  11/11/2014
 *  Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/logging/LogHandler.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $ 
 *  Log:    $Log: LogHandler.java,v $
 *  Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 *  Log:    Mulitple updates
 *  Log:   
 *  Log:    Revision 1.3  2014/11/12 21:00:24  arnoldmd
 *  Log:    This a New Module
 *  Log:   
 *  Log:    Revision 1.2  2014/11/12 20:06:23  arnoldmd
 *  Log:    no message
 *  Log:   
 */

package com.arnoldmd.hadoop.logging;

import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 *
 * @author adajao2
 */
class LogHandler extends StreamHandler {
 
    @Override
    public void publish(LogRecord record) {
        //add own logic to publish
        super.publish(record);
    }
 
 
    @Override
    public void flush() {
        super.flush();
    }
 
 
    @Override
    public void close() throws SecurityException {
        super.close();
    }
 
}
