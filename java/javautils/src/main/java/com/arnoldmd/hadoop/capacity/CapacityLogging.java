/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 12/16/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/capacity/CapacityLogging.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: CapacityLogging.java,v $
 * <Description>, ArnoldMD 12/16/2014   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 12/16/2014   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 12/16/2014   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.capacity;

import com.arnoldmd.hadoop.system.SystemUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Capacity logging for Running applications for Ops Monitoring
 * 
 * @author adajao2
 * @version 1.05
 *
 */
public class CapacityLogging {

	public CapacityLogging() {
	}

	/**
	 * 
	 * @param duration the run duration of the application
	 * @param region the region being processed
	 * @param operation the operation application is running
	 * @param adaptor the adaptor name
	 * @param measurement the measurement name
	 * @param object the object name
	 * @param filesize the size of the file
	 * @param numrows the number of rows inserted/processed
	 * @param counters the counter used
	 * @return the comma separated format for table loading to hive
	 */

	public String getCapacityLine(String duration, String region,
			String operation, String adaptor, String measurement,
			String object, String filesize, String numrows, String counters) {
		String updatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.format(new Date());
		String pid = SystemUtils.getPid();
		String host = SystemUtils.getHost();
		return updatetime + "," + pid + "," + updatetime + "," + duration + ","
				+ region + "," + host + "," + operation + "," + adaptor + ","
				+ measurement + "," + object + "," + filesize + "," + numrows
				+ "," + counters;
	}

}
