/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    $Log: HdfsClient.java,v $
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Mulitple updates
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.2 2014/12/09
 * 18:25:49 arnoldmd
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    updated Mail and HDFS
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1 2014/11/25
 * 02:22:47 arnoldmd
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Update for HDFS utilities amd
 * merging
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/HdfsClient.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 */
package com.arnoldmd.hadoop.hdfs;

import com.arnoldmd.hadoop.logging.Log4jAppUtils;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author adajao2
 */
public class HdfsClient {

	private final String APP_NAME = HdfsContent.class.getSimpleName();
	Logger log = Logger.getLogger(APP_NAME);

	public void TestHdfsStat() {
		new Log4jAppUtils().setConsoleLogThreshold(Level.DEBUG);
		log.debug("start Test");
		String SourcePath = "/user/omc/data/databases/preproduction";
		Configuration config = new Configuration();
		config.set("fs.defaultFS", "hdfs://namenodeip");

		try {
			workAround();

			Hdfs hfs = new Hdfs(true);
			hfs.hadoopls(SourcePath, true, "");

			List<HdfsStat> hdfsfilestat = hfs.getHdfsFilesStat();

			for (HdfsStat hdfsfilestat1 : hdfsfilestat) {
				System.out.println(hdfsfilestat1.getFileName_() + ","
						+ hdfsfilestat1.replicationFactor_);

			}
		} catch (IOException ex) {
			log.fatal("IOException" + ex);

		}


	}



	public void DirCount() {
		new Log4jAppUtils().setConsoleLogThreshold(Level.DEBUG);
		log.debug("start Test");
		String SourcePath = "/user/omc/data/databases/preproduction/tables/misc/tpimkpi/tcmkpi_p_4gkpi_cell_day";
		Configuration config = new Configuration();
		config.set("fs.defaultFS", "hdfs://10.46.50.245");

		try {
			workAround();
			Boolean activeNamenode = true;

			Hdfs hfs = new Hdfs(activeNamenode);
			hfs.hadoopls(SourcePath, true, "pdate");
			HdfsContent hc = hfs.getHdfsContents();
			System.out.println("dircount " + hc.getPathCount()
					+ " file Count: " + hc.getFileCount());

			System.out.println("oldest file " + hfs.getOldestFile());
			for (String file : hc.getPaths()) {
				System.out.println("files: " + file);
				System.out.println(" file Count: "
						+ hc.getPathSize(file).toString());
			}

			Map<String, Integer> pfc = hc.getPathFileCount(6);
			for (String key : pfc.keySet()) {
				System.out.println("drive" + key + " no of file: "
						+ pfc.get(key));

			}

		} catch (IOException ex) {

			log.fatal("IOException" + ex);

		}

	}

	/**
	 *
	 * @throws IOException
	 */
	public void workAround() throws IOException {
		// start work around//
		File workaround = new File(".");
		System.getProperties().put("hadoop.home.dir",
				workaround.getAbsolutePath());
		new File("./bin").mkdirs();
		new File("./bin/winutils.exe").createNewFile();
		// * end work around//
	}

}
