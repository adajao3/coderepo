/*
 * Copyright 2015 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 01/21/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/localfs/LocalFsUtils.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: LocalFsUtils.java,v $
 * <Description>, ArnoldMD 01/21/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 01/21/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 01/21/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.localfs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class LocalFsUtils {

    private static final String APP_SVN_REV = "$Version: 1.0$".replaceAll("\\$", "").replace("sion", "") + " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
    private static final String APP_NAME = LocalFsUtils.class.getSimpleName();
    private static Logger log = Logger.getLogger(APP_NAME);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String sourcePath = "C:/Temp";
        String regexPattern = ".*\\d+.log$";
        List<String> files = getFileList(sourcePath, regexPattern);
        for (String file : files) {
            System.out.println("file: " + file);

        }
    }

    public static void SaveArrayToFile(List< String> ofArrayList, String FileName) {

        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(FileName));
            for (String line : ofArrayList) {
                br.write(line + "\n");
                System.out.println(line);
            }

            br.close();
        } catch (IOException ex) {
            log.fatal("error filesave:" + ex.getMessage());
        }
    }

    public static boolean moveLsFile(String SourceFile, String DestinationFile) {
        //File Name
        File sf = new File(SourceFile);
        return sf.renameTo(new File(DestinationFile));
    }

    public static boolean deleteLsFile(String SourceFile) {
        //File Name
        File sf = new File(SourceFile);
        return sf.delete();
    }

    public static String getLsFileName(String FileFullPath) {
        //File Name
        File f = new File(FileFullPath);
        return f.getName();
    }

    public static String getLsPathName(String FileFullPath) {
        //File Name
        File f = new File(FileFullPath);
        log.debug("FileFullPath:" + FileFullPath + " ParentPathName:" + f.getParent());
        return f.getParent();
    }

    public static String getLsParentPathName(String FileFullPath) {
        //File Name
        File f = new File(FileFullPath);
        log.debug("FileFullPath:" + FileFullPath + " ParentPathName:" + new File(f.getParent()).getParent());
        return new File(f.getParent()).getParent();
    }

        public static Long getLastModified(String FileFullPath) {
        //File Name
        File f = new File(FileFullPath);
        log.debug("FileFullPath:" + FileFullPath + " ParentPathName:" + new File(f.getParent()).getParent());
        return f.lastModified();
    }
    
    public static List<String> getFileList(String sourcePath, String regexPattern) {
        List<String> fileList = new ArrayList<String>();
        Path basePath = Paths.get(sourcePath);
//        System.out.println("All document files[Using Filter class]");
        log.debug("getFileList:ssource: " + sourcePath + " filte:" + regexPattern);
        DirectoryStream.Filter<Path> documentFilter = buildRegexFilter(regexPattern);
        try (DirectoryStream<Path> pathList = Files.newDirectoryStream(basePath,
                documentFilter)) {
            for (Path path : pathList) {
//                System.out.println(path.toString());
                log.debug("getFileList:" + path.toString());
                fileList.add(path.toString());
            }

        } catch (IOException e) {
            log.fatal("getFileList: IOException " + e);
//            e.printStackTrace();
            return null;
        }
        return fileList;
    }

    public static DirectoryStream.Filter<Path> buildRegexFilter(String pattern) {
        final PathMatcher pathMatcher = getPathMatcher("regex:" + pattern);
        return new DirectoryStream.Filter<Path>() {
            @Override
            public boolean accept(Path entry) throws IOException {
                return pathMatcher.matches(entry);
            }
        };
    }

    private static PathMatcher getPathMatcher(String pattern) {
        return FileSystems.getDefault().getPathMatcher(pattern);
    }
    
   public static void delLsFolder(String folder) {
        try {
            Path path = Paths.get(folder);
            Path walkFileTree = Files.walkFileTree(path, new FileVisitor<Object>() {

                @Override
                public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {
                    System.out.println("Deleting file: " + file);
                    Files.delete((Path) file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Object file, IOException exc) throws IOException {
                    System.out.println(exc.toString());
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
                     System.out.println("deleting directory :" + dir);
                    Files.delete((Path) dir);
                    return FileVisitResult.CONTINUE;
                }

            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
