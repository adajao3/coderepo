/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/12/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/utils/getMainClass.java,v
 * 1.1 2014/12/09 18:25:49 arnoldmd Exp $    Log:    $Log: getMainClass.java,v
 * $
 * <Description>, ArnoldMD 11/12/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/utils/getMainClass.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    Revision
 * 1.1 2014/12/09 18:25:49 arnoldmd
 * <Description>, ArnoldMD 11/12/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/utils/getMainClass.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    updated
 * Mail and HDFS
 * <Description>, ArnoldMD 11/12/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/utils/getMainClass.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:   
 */
package com.arnoldmd.hadoop.utils;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 *
 * @author adajao2
 */
public class getMainClass {

    /**
     * Get The Main Class of the Java script being executed. Alternative to
     * application Name.
     *
     * @return NameOfTheJavaMainClass
     */
    public static String getMainClassNMame() {
        ThreadMXBean temp = ManagementFactory.getThreadMXBean();
        ThreadInfo t = temp.getThreadInfo(1, Integer.MAX_VALUE);
//        System.out.println("Name: " + t.getThreadName());
        StackTraceElement st[] = t.getStackTrace();
        System.out.println("Main Class Name: " + st[st.length - 1].getClassName());
        return st[st.length - 1].getClassName();
    }

    public static String getMainClassShortName() {
        ThreadMXBean temp = ManagementFactory.getThreadMXBean();
        ThreadInfo t = temp.getThreadInfo(1, Integer.MAX_VALUE);
//        System.out.println("Name: " + t.getThreadName());
        StackTraceElement st[] = t.getStackTrace();
        System.out.println("Main Class Name: " + extractSimpleClassName(st[st.length - 1].getClassName()));
        return extractSimpleClassName(st[st.length - 1].getClassName());
    }
//  

    public static String extractSimpleClassName(String fullClassName) {

        if ((null == fullClassName) || ("".equals(fullClassName))) {
            return "";
        }

        int lastDot = fullClassName.lastIndexOf('.');
        if (0 > lastDot) {
            return fullClassName;
        }

        return fullClassName.substring(++lastDot);
    }
}
