/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/Hdfs.java,v 1.1
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:    $Log: Hdfs.java,v $ 2014/11/25
 * 02:22:47 arnoldmd Exp $    Log:    Revision 1.1 2015/03/25 23:55:07 arnoldmd
 * 2014/11/25 02:22:47 arnoldmd Exp $    Log:    Mulitple updates 2014/11/25
 * 02:22:47 arnoldmd Exp $    Log:    2014/11/25 02:22:47 arnoldmd Exp $   
 * Log:    Revision 1.2 2014/12/09 18:25:49 arnoldmd 2014/11/25 02:22:47
 * arnoldmd Exp $    Log:    updated Mail and HDFS 2014/11/25 02:22:47 arnoldmd
 * Exp $    Log:   
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/Hdfs.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1 2014/11/25
 * 02:22:47 arnoldmd
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/Hdfs.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Update for HDFS utilities amd
 * merging
 * <Description>, ArnoldMD 11/20/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/hdfs/Hdfs.java,v 1.2
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:   
 */
package com.arnoldmd.hadoop.hdfs;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.PrivilegedExceptionAction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.log4j.Logger;

/**
 * Utilities for reading and writing to HDFS
 * 
 * @author adajao2
 */
public class Hdfs {

	private  final String APP_SVN_REV = "$Version: 1.4$".replaceAll(
			"\\$", "").replace("sion", "")
			+ " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
	private  final String APP_NAME = Hdfs.class.getSimpleName();
	// initializing the logger
	 Logger log = Logger.getLogger(APP_NAME);
	/**
     *
     */
	public  final SimpleDateFormat dateForm = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");
	private HdfsContent hc_ = new HdfsContent();
	private int PathCount_ = 0;
	private int FileCount_ = 0;
	private int ErrorCount_ = 0;
	private List<String> Files_ = new ArrayList<String>();
	private List<String> Errors_ = new ArrayList<String>();
	private List<String> Paths_ = new ArrayList<String>();
	private String OldestFile_ = "";
	private String activeNamenode_ = "";
	private double ModificationTime_ = 0;
	private FileStatus sourcePath_ = null;
	private  FileSystem hadoopFs_ = null;
	Map<String, Integer> PathFileCount_ = new HashMap<String, Integer>();
	Map<String, Long> PathFileSize_ = new HashMap<String, Long>();
	Map<String, String> PathOldestFile_ = new HashMap<String, String>();
	List<HdfsStat> HStatList = new ArrayList<HdfsStat>();

	/**
	 * Check for Active Name Node
	 * 
	 * @param activeNamenode
	 */
	public Hdfs(String activeNamenode) {

		Configuration config = new Configuration();
		config.set("fs.defaultFS", activeNamenode);
		activeNamenode_ = activeNamenode;
		log.info(APP_NAME + APP_SVN_REV + " start");
		try {
			hadoopFs_ = FileSystem.get(config);
		} catch (IOException ex) {
			log.fatal("IOException " + ex);
		}

	}

	/**
	 *
	 * @param activeNamenode
	 */
	public Hdfs(Boolean getNamenode) {
		String activeNamenode = "hdfs://<NameNodeIP>";
		Configuration config = new Configuration();
		config.set("fs.defaultFS", activeNamenode);
		activeNamenode_ = activeNamenode;
		// logIf.UpdateLog(Level.CONFIG, "fs.defaultFS:" + activeNamenode);
		// System.out.println("start");
		log.info(APP_NAME + APP_SVN_REV + " start");

		try {
			hadoopFs_ = FileSystem.get(config);
			hadoopls("/user", false, "");

		} catch (IOException ex) {
			try {
				System.out.println("NameNode2");
				log.warn("Active Name Node is Name Node2 NameNode2IP");
				activeNamenode = "hdfs://NameNode2IP";
				config = new Configuration();
				config.set("fs.defaultFS", activeNamenode);
				activeNamenode_ = activeNamenode;
				hadoopFs_ = FileSystem.get(config);

			} catch (IOException ex1) {
				log.fatal("IOException " + ex);
			}
		}

	}

	/**
	 * Get HDFS Content
	 * 
	 * @return "HdfsContent" object
	 */
	public HdfsContent getHdfsContent() {
		hc_.NewHdfsContent(PathCount_, FileCount_, ErrorCount_, Files_, Paths_,
				Errors_, PathFileCount_, PathOldestFile_);
		return hc_;
	}

	/**
	 * Get HDFS Contents
	 * 
	 * @return
	 */
	public HdfsContent getHdfsContents() {
		hc_.NewHdfsContent(PathCount_, FileCount_, ErrorCount_, Files_, Paths_,
				Errors_, PathFileCount_, PathOldestFile_, PathFileSize_);
		return hc_;
	}

	/**
	 * Get the oldest file
	 * 
	 * @return oldest hdfs file
	 */
	public String getOldestFile() {
		return OldestFile_;
	}

	/**
	 * Retrieve File Statistics (replication, owner, group,size, date modified,
	 * fileName)
	 * 
	 * @return
	 */
	public List<HdfsStat> getHdfsFilesStat() {
		return HStatList;
	}

	/**
	 * Recursive Scan on HDFS path
	 * 
	 * @param SourcePath
	 * @param recursive
	 * @param filter
	 * @throws IOException
	 * @returns
	 */
	public void hadoopls(String SourcePath, boolean recursive, String filter)
			throws IOException {
		sourcePath_ = hadoopFs_.getFileStatus(new Path(SourcePath));
		final String cmd_ = recursive ? "lsr" : "ls";
		final String filter_ = filter.isEmpty() ? "/" : filter;
		final FileStatus[] items = shellListStatus(cmd_, hadoopFs_, sourcePath_);
		if (items == null) {

			ErrorCount_++;
			Errors_.add(sourcePath_ + " error items null");
		} else {

			int maxReplication = 3, maxLen = 10, maxOwner = 0, maxGroup = 0;

			for (int i = 0; i < items.length; i++) {
				FileStatus stat = items[i];
				int replication = String.valueOf(stat.getReplication())
						.length();
				int len = String.valueOf(stat.getLen()).length();
				int owner = String.valueOf(stat.getOwner()).length();
				int group = String.valueOf(stat.getGroup()).length();
//				String mdate = dateForm.format(new Date(stat
//						.getModificationTime()));

				if (replication > maxReplication) {
					maxReplication = replication;
				}
				if (len > maxLen) {
					maxLen = len;
				}
				if (owner > maxOwner) {
					maxOwner = owner;
				}
				if (group > maxGroup) {
					maxGroup = group;
				}
			}

			for (int i = 0; i < items.length; i++) {
				FileStatus stat = items[i];
				Path cur = stat.getPath();
				// filter
				if (stat.getPath().toString().contains(filter_)) {
					int replication = stat.getReplication();
					long len = stat.getLen();
					String owner = stat.getOwner();
					String group = stat.getGroup();
					String mdate = dateForm.format(new Date(stat
							.getModificationTime()));

					if (stat.isDir()) {
						// dir
						Paths_.add(cur.toUri().getPath());
						PathCount_++;
						ModificationTime_ = 0;
						log.debug("Dir: " + cur.toUri().getPath()
								+ " Dir Count:" + PathCount_);
					} else {
						// for files
						Long FileSize = stat.getLen();
						String fileName = cur.toUri().getPath();
						Files_.add(cur.toUri().getPath());
						FileCount_++;
						// add to hdfs stat
						HStatList.add(new HdfsStat(replication, owner, group,
								len, mdate, fileName));
						String hdfsPath = cur.getParent().toUri().getPath();
						// FileCount for Directory
						if (PathFileCount_.containsKey(hdfsPath)) {
							PathFileCount_.put(hdfsPath,
									PathFileCount_.get(hdfsPath) + 1);
							PathFileSize_.put(hdfsPath,
									PathFileSize_.get(hdfsPath) + FileSize);
							log.debug("hdfsPath" + hdfsPath + " File Count "
									+ PathFileCount_.get(hdfsPath) + 1
									+ " fileSize "
									+ PathFileSize_.get(hdfsPath) + FileSize);
						} else {
							PathFileCount_.put(hdfsPath, 1);
							PathFileSize_.put(hdfsPath, FileSize);
							log.debug("hdfsPath" + hdfsPath + " File Count "
									+ 1 + " fileSize " + FileSize);
						}

						// oldest file
						if (stat.getModificationTime() > ModificationTime_) {
							log.debug("ModificationTime_ "
									+ stat.getModificationTime() + " is older "
									+ ModificationTime_);
							ModificationTime_ = stat.getModificationTime();
							OldestFile_ = cur.toUri().getPath();
							PathOldestFile_.put(hdfsPath, OldestFile_);

						}

					}

				}
				if (recursive && stat.isDir()) {
					hadoopls(stat.getPath().toUri().getPath(), recursive,
							filter);
				}
			}
		}

	}

	/**
	 * Parse file status from Shell
	 * @param command
	 * @param fileSystem
	 * @param SourcePath
	 * @return
	 */
	private  FileStatus[] shellListStatus(String command,
			FileSystem fileSystem, FileStatus SourcePath) {
		if (!SourcePath.isDir()) {
			FileStatus[] files = { SourcePath };
			return files;
		}
		Path path = SourcePath.getPath();
		try {
			FileStatus[] files = fileSystem.listStatus(path);
			if (files == null) {
				System.err.println(command + ": could not get listing for '"
						+ path + "'");
			}
			return files;
		} catch (IOException e) {
			System.err.println(command + ": could not get get listing for '"
					+ path + "' : " + e.getMessage().split("\n")[0]);
		}
		return null;
	}

	/**
	 * Merge Files in HDFS
	 * @param sourcePath
	 * @param targetPathFile
	 * @param bufferSize
	 *            typical size is 1024 or 8192
	 * @return boolean 
	 * @throws Exception
	 */
	public  boolean hdfsMerge(String sourcePath, String targetPathFile,
			int bufferSize) throws Exception {
		Path InFile = new Path(sourcePath);
		Path OutFile = new Path(targetPathFile);
		try {
			FSDataOutputStream OutStream = null;

			if (hadoopFs_.exists(OutFile)) {
				// System.out.println("Output File " + OutFile +
				// " exists. Appending!");
				log.debug("Output File: " + OutFile + " exists. Appending!");
				OutStream = hadoopFs_.append(OutFile);
			} else {
				// System.out.println("Output File " + OutFile
				// + " does NOT exist! Creating!");
				log.debug("Output File: " + OutFile
						+ " does NOT exist! Creating!");
				OutStream = hadoopFs_.create(OutFile);
			}
			FileStatus[] hdfsFileList = hadoopFs_.listStatus(InFile);

			for (FileStatus f : hdfsFileList) {
				if (!hadoopFs_.exists(f.getPath())) {
					System.out.println("Input file: " + f.getPath().toString()
							+ " does not exist!");
					log.error("Input file: " + f.getPath().toString()
							+ " does not exist!");
					return false;
				}
				try {
					FSDataInputStream InStream = hadoopFs_.open(f.getPath());
					byte buffer[] = new byte[bufferSize];
					int bytesread = 0;
					while ((bytesread = InStream.read(buffer)) > 0) {
						OutStream.write(buffer, 0, bytesread);
					}
					InStream.close();
				} catch (Exception e) {
					// System.out.println("exception" + e.toString());
					log.fatal("exception" + e.toString());
					return false;
				}

			}

			OutStream.close();
		} catch (Exception e) {
			// System.out.println("exception" + e.toString());
			log.fatal("exception" + e.toString());
			return false;
		}


		return true;
	}

	/**
	 *
	 * @param targetPathFile
	 * @param FileContents
	 * @return
	 * @throws Exception
	 */
	public  boolean hdfsWriteFile(String targetPathFile,
			List<String> FileContents) throws Exception {
		BufferedWriter br;
		Path OutFile = new Path(targetPathFile);
		try {

			if (hadoopFs_.exists(OutFile)) {
				// System.out.println("Output File " + OutFile +
				// " exists. Appending!");
				log.info("Output File " + OutFile + " exists. Appending!");
				br = new BufferedWriter(new OutputStreamWriter(
						hadoopFs_.append(OutFile)));
			} else {
				// System.out.println("Output File " + OutFile
				// + " does NOT exist! Creating!");
				log.info("Output File " + OutFile
						+ " does NOT exist! Creating!");
				br = new BufferedWriter(new OutputStreamWriter(
						hadoopFs_.create(OutFile)));
			}

			log.info("File Size:" + Integer.toString(FileContents.size()));
			for (String f : FileContents) {
				try {
					br.write(f + "\n");
					log.trace(f);
				} catch (Exception e) {
					// System.out.println("exception" + e.toString());
					log.error("exception" + e.toString());
					return false;
				}

			}

			br.close();
		} catch (Exception e) {
			System.out.println("exception" + e.toString());
			return false;
		}

		// if (!hdfs.delete(InFile, false)) {
		// System.out.println("Could not delete file " + InFile);
		// }
		return true;
	}

	/**
	 *
	 * @param targetPathFile
	 * @param FileContents
	 * @param append
	 * @return
	 * @throws Exception
	 */
	public  boolean hdfsWriteFile(String targetPathFile,
			List<String> FileContents, boolean append) throws Exception {
		BufferedWriter br;
		Path OutFile = new Path(targetPathFile);
		try {

			if (hadoopFs_.exists(OutFile) && append) {
				System.out.println("Output File " + OutFile
						+ " exists. Appending!");
				br = new BufferedWriter(new OutputStreamWriter(
						hadoopFs_.append(OutFile)));
			} else {
				System.out.println("Output File " + OutFile
						+ " does NOT exist! Creating!");
				br = new BufferedWriter(new OutputStreamWriter(
						hadoopFs_.create(OutFile, true)));
			}

			for (String f : FileContents) {

				try {
					br.write(f + "\n");
				} catch (Exception e) {
					System.out.println("exception" + e.toString());
					return false;
				}

			}
			// for (String f : FileContents) {
			//
			// try {
			// br.write(f + "\n");
			// } catch (Exception e) {
			// System.out.println("exception" + e.toString());
			// return false;
			// }
			//
			// }
			br.close();
		} catch (Exception e) {
			System.out.println("exception" + e.toString());
			return false;
		}

		// if (!hdfs.delete(InFile, false)) {
		// System.out.println("Could not delete file " + InFile);
		// }
		return true;
	}

	/**
	 *
	 * @param sourcePath
	 *            can contain wild card
	 * @throws IOException
	 *             Deletes single file or all files (*) in the directory
	 */
	public void hdfsDelete(final String sourcePath) throws IOException {

		UserGroupInformation ugi = UserGroupInformation
				.createRemoteUser("hdfs");
		try {
			ugi.doAs(new PrivilegedExceptionAction<Void>() {
				public Void run() throws Exception {
					Configuration config = new Configuration();
					config.set("fs.defaultFS", activeNamenode_);
					FileSystem hdfs = FileSystem.get(config);

					// check for wildcard
					if (sourcePath.contains("*")) {
						String[] matchStrings = sourcePath.split("\\*");
						StringBuilder sb = new StringBuilder();
						for (int i = 0; i < matchStrings.length - 1; i++) {
							sb.append("(").append(matchStrings[i])
									.append(".*)");
						}

						sb.append(matchStrings[matchStrings.length - 1]);
						Pattern pattern = Pattern.compile(sb.toString());
						FileStatus[] hdfsFileList = hdfs.listStatus(new Path(
								sourcePath).getParent());
						for (FileStatus f : hdfsFileList) {
							Matcher m = pattern.matcher(f.getPath().toString());
							if (m.find()) {
								// System.out.println("matches: " + m.group(1));
								// delete
								if (hdfs.delete(f.getPath(), false)) {
									// System.out.println("hdfs delete : " +
									// f.getPath().toString() + " complete!");
									log.debug("hdfs delete : "
											+ f.getPath().toString()
											+ " complete!");
								} else {
									// System.out.println("Error hdfs delete : "
									// + f.getPath().toString() + " problem!");
									log.fatal("Error hdfs delete : "
											+ f.getPath().toString()
											+ " problem!");
								}
							}
						}
					} else {
						// single file
						Path sourceDir = new Path(sourcePath);
						if (!hdfs.exists(sourceDir)) {
							System.out.println("Input file: "
									+ sourceDir.toString() + " does not exist!");
						}
						if (hdfs.delete(sourceDir, false)) {
							System.out.println("hdfs delete : "
									+ sourceDir.toString() + " complete!");
						} else {
							System.out.println("Error hdfs delete : "
									+ sourceDir.toString() + " problem!");
						}
					}
					System.out.println("done delete");
					// move

					return null;
				}

			});
		} catch (InterruptedException ex) {
			// logIf.UpdateLog(Level.SEVERE, "InterruptedException" + ex);
		}

	}

	/**
	 *
	 * @param sourcePath
	 *            path witl be scan recursive
	 * @param daysOffset
	 *            days from today to be deleted
	 * @throws IOException
	 *             Deletes single file or all files (*) in the directory
	 */
	public void hdfsDelete(final String sourcePath, final int daysOffset,
			final boolean recursive) throws IOException {

		UserGroupInformation ugi = UserGroupInformation
				.createRemoteUser("hdfs");
		try {
			ugi.doAs(new PrivilegedExceptionAction<Void>() {
				public Void run() throws Exception {
					Configuration config = new Configuration();
					config.set("fs.defaultFS", activeNamenode_);
					FileSystem hdfs = FileSystem.get(config);

					// check for wildcard
					FileStatus[] hdfsFileList = hdfs.listStatus(new Path(
							sourcePath));
					for (FileStatus f : hdfsFileList) {
						String mdate = dateForm.format(new Date(f
								.getModificationTime()));
						long cutoff = System.currentTimeMillis()
								- (24 * 60 * 60 * 1000 * daysOffset);
						if (f.getModificationTime() < cutoff) {
							if (recursive && f.isDir()) {
								hdfsDelete(f.getPath().toUri().getPath(),
										daysOffset, recursive);
								if (!hdfs.delete(f.getPath(), false)) {
									log.fatal("Cannot Deleted Folder: "
											+ f.getPath().toString()
											+ " mdate: " + mdate);
								}
								log.info("Deleted folder: "
										+ f.getPath().toString() + " mdate: "
										+ mdate);
							} else {
								if (!hdfs.delete(f.getPath(), false)) {
									log.fatal("Cannot Deleted file: "
											+ f.getPath().toString()
											+ " mdate: " + mdate);
								}
								log.debug("Deleted file: "
										+ f.getPath().toString() + " mdate: "
										+ mdate);

							}
						} else {
							log.debug("Skip file:" + f.getPath().toString()
									+ " cutoff:" + cutoff + " mod:"
									+ f.getModificationTime() + "mdate:"
									+ mdate);
							System.out.println("Skip file:"
									+ f.getPath().toString() + " cutoff:"
									+ cutoff + " mod:"
									+ f.getModificationTime() + "mdate:"
									+ mdate);
						}

					}

					return null;
				}

			});
		} catch (InterruptedException ex) {
			// logIf.UpdateLog(Level.SEVERE, "InterruptedException" + ex);
		}

	}

	public void HdfsMove(final String sourceFile, final String targetFile) {
		UserGroupInformation ugi = UserGroupInformation
				.createRemoteUser("hdfs");
		try {
			ugi.doAs(new PrivilegedExceptionAction<Void>() {
				public Void run() throws Exception {
					Configuration config = new Configuration();
					config.set("fs.defaultFS", activeNamenode_);
					FileSystem hdfs = FileSystem.get(config);

					Path sFile = new Path(sourceFile);
					Path tFile = new Path(targetFile);

					// move
					if (hdfs.rename(sFile, tFile)) {
						log.trace("hdfs move from : " + sFile + "\n\tto "
								+ tFile);
						System.out.println("hdfs move from : " + sFile
								+ "\n\tto " + tFile);
					} else {
						log.warn("did not complete hdfs move from : " + sFile
								+ "\n\tto " + tFile);
						System.out.println("Error hdfs move from : " + sFile
								+ "\n\tto " + tFile);

					}
					return null;
				}

			});
		} catch (InterruptedException ex) {
			// logIf.UpdateLog(Level.SEVERE, "InterruptedException" + ex);
			log.fatal("ERROR:" + ex);
		} catch (IOException ex) {
			// ex.printStackTrace();
			log.fatal("ERROR:" + ex);
		}

	}

	/**
	 *
	 * @param sourcePath
	 *            source
	 * @param targetPathFile
	 * @param hdfsTempPath
	 * @throws IOException
	 *             deletes sll files from the @sourcePath then copy the merged
	 *             file from @hdfsTempPath to
	 * @sourcePath
	 */
	public void moveMerged(final String sourcePath,
			final String targetPathFile, final String hdfsTempPath)
			throws IOException {

		UserGroupInformation ugi = UserGroupInformation
				.createRemoteUser("hdfs");
		try {
			ugi.doAs(new PrivilegedExceptionAction<Void>() {
				public Void run() throws Exception {
					Configuration config = new Configuration();
					config.set("fs.defaultFS", activeNamenode_);
					FileSystem hdfs = FileSystem.get(config);

					Path sourceDir = new Path(sourcePath);
					Path targetFile = new Path(sourcePath + "/"
							+ targetPathFile);
					Path tempFile = new Path(hdfsTempPath + "/"
							+ targetPathFile);
					FileStatus[] hdfsFileList = hdfs.listStatus(sourceDir);

					for (FileStatus f : hdfsFileList) {
						if (!hdfs.exists(f.getPath())) {
							System.out.println("Input file: "
									+ f.getPath().toString()
									+ " does not exist!");

						}

						if (hdfs.delete(f.getPath(), false)) {
							System.out.println("hdfs delete : "
									+ f.getPath().toString() + " complete!");
						} else {
							System.out.println("Error hdfs delete : "
									+ f.getPath().toString() + " problem!");
						}
					}
					System.out.println("done delete");
					// move
					if (hdfs.rename(tempFile, targetFile)) {
						System.out.println("hdfs move from : " + tempFile
								+ "\n\tto " + targetFile);
					} else {
						System.out.println("Error hdfs move from : " + tempFile
								+ "\n\tto " + targetFile);
					}
					return null;
				}

			});
		} catch (InterruptedException ex) {
			// logIf.UpdateLog(Level.SEVERE, "InterruptedException" + ex);
		}

	}

}
