/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/25/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/date/TimeConversion.java,v
 * 1.1 2014/12/09 18:25:49 arnoldmd Exp $    Log:    $Log:
 * TimeConversion.java,v $
 * <Description>, ArnoldMD 11/25/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/date/TimeConversion.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    Revision
 * 1.1 2014/12/09 18:25:49 arnoldmd
 * <Description>, ArnoldMD 11/25/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/date/TimeConversion.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    updated
 * Mail and HDFS
 * <Description>, ArnoldMD 11/25/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/date/TimeConversion.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:   
 */
package com.arnoldmd.hadoop.date;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Utilities for converting time, date runtime
 * 
 * @author adajao2
 *
 */
public class TimeConversion {

	private final String APP_NAME = TimeConversion.class.getSimpleName();
	private Logger log = Logger.getLogger(APP_NAME);

	/**
	 * Compute the runtime from start to end time
	 * 
	 * @param dateStart
	 * @param dateStop
	 * @param dateFormat
	 * @return runtime in seconds
	 */
	public int getRunTimeSec(String dateStart, String dateStop,
			String dateFormat) {

		DateTimeFormatter format = DateTimeFormat.forPattern(dateFormat);
		DateTime dt1 = format.parseDateTime(dateStart);
		DateTime dt2 = format.parseDateTime(dateStop);
		log.debug(Seconds.secondsBetween(dt1, dt2).getSeconds()
				+ " seconds runtime");
		int runtime = Seconds.secondsBetween(dt1, dt2).getSeconds();
		return runtime;
	}

	/**
	 * Formats Date and time to specific format
	 * 
	 * @param dateTime the date and time
	 * @param dateFormat  the date format of source time
	 * @param returnFormat
	 *            the date format of output
	 * @return
	 */
	public String getDateTime(String dateTime, String dateFormat,
			String returnFormat) {
		DateTimeFormatter format = DateTimeFormat.forPattern(dateFormat);
		DateTime dt1 = format.parseDateTime(dateTime);
		return dt1.toString(returnFormat);

	}

	/**
	 * 
	 * @param dateFormat
	 * @return current date and time in given format
	 */
	public String getDateTime(String dateFormat) {
		DateTime today = new DateTime();
		String dt1 = today.toString(dateFormat);
		return dt1;
	}

	/**
	 * Convert time to GMT
	 * @param dateFormat
	 * @return time in GMT
	 */
	public String getDateTimeGMT(String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date date = new Date();
		String s = sdf.format(date);
		return s;
		// System.out.println("GMT: " + s);
	}



	/**
	 * format Milliseconds to HH:MM:SS.SS
	 * @param totalMilliSeconds
	 * @return
	 */
	public String getTimeConversion(long totalMilliSeconds) {
		double totalSeconds = totalMilliSeconds / 1000.0;
		int MINUTES_IN_AN_HOUR = 60;
		int SECONDS_IN_A_MINUTE = 60;
		int hours = new Double(totalSeconds).intValue() / MINUTES_IN_AN_HOUR
				/ SECONDS_IN_A_MINUTE;
		int minutes = (new Double(totalSeconds).intValue() - (hoursToSeconds(hours)))
				/ SECONDS_IN_A_MINUTE;
		DecimalFormat df = new DecimalFormat("00.00");
		double seconds = totalSeconds
				- ((hoursToSeconds(hours)) + (minutesToSeconds(minutes)));
		// System.out.println("s:" + seconds);
		return String.format("%02d", hours) + ":"
				+ String.format("%02d", minutes) + ":" + df.format(seconds);
	}

	/**
	 * Cover Hours to Secs
	 * @param hours
	 * @return hours
	 */
	private int hoursToSeconds(int hours) {
		return hours * 60 * 60;
	}

	/**
	 * 
	 * @param minutes
	 * @return
	 */
	private int minutesToSeconds(int minutes) {
		return minutes * 60;
	}

	/**
	 * Converts HDFS time stamp to human readable format
	 * @param fsDate
	 * @param fsFormat
	 * @return
	 */
	public String getHdfsTimeStamp(String fsDate, String fsFormat) {
		if (fsFormat.equals("yyyyMMddHH")) {
			String year = fsDate.substring(0, 4);
			String month = fsDate.substring(4, 6);
			String day = fsDate.substring(6, 8);
			String hour = fsDate.substring(8, 10);
			// "yyyy-MM-dd HH:mm:ss"
			log.trace("getHdfsTimeStamp result: " + year + "-" + month + "-"
					+ day + " " + hour + ":00:00");
			return year + "-" + month + "-" + day + " " + hour + ":00:00";

		} else if (fsFormat.equals("yyyyMMddHHmmss")) {
			String year = fsDate.substring(0, 4);
			String month = fsDate.substring(4, 6);
			String day = fsDate.substring(6, 8);
			String hour = fsDate.substring(8, 10);
			String minute = fsDate.substring(10, 12);
			String sec = fsDate.substring(12, 14);
			// "yyyy-MM-dd HH:mm:ss"
			log.trace("getHdfsTimeStamp result: " + year + "-" + month + "-"
					+ day + " " + hour + ":" + minute + ":" + sec);
			return year + "-" + month + "-" + day + " " + hour + ":" + minute
					+ ":" + sec;
		}
		log.error("cannot convert " + fsDate + " to " + fsFormat);
		return null;
	}

	/**
	 * Convert format Seconds to HH:MM:SS.SS
	 * @param totalSeconds
	 * @return
	 */
	public String getTimeConversionSec(int totalSeconds) {
		int MINUTES_IN_AN_HOUR = 60;
		int SECONDS_IN_A_MINUTE = 60;
		int hours = totalSeconds / MINUTES_IN_AN_HOUR / SECONDS_IN_A_MINUTE;
		int minutes = (totalSeconds - (hoursToSeconds(hours)))
				/ SECONDS_IN_A_MINUTE;
		int seconds = totalSeconds
				- ((hoursToSeconds(hours)) + (minutesToSeconds(minutes)));

		return "hh:mm:ss " + String.format("%02d", hours) + ":"
				+ String.format("%02d", minutes) + ":"
				+ String.format("%02d", seconds);
	}

}
