/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 02/19/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/config/TpimUtilsConfig.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: TpimUtilsConfig.java,v $
 * <Description>, ArnoldMD 02/19/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 02/19/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 02/19/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.config;

import java.util.HashMap;
/**
 * Reading  config from JSON File
 * @author adajao2
 *
 */
public class AppUtilsConfig {

	//fields
    private HashMap<String, String> preprodHosts;
    private String pwd;
    private String usr;

    AppUtilsConfig() {
    }

    //constructor
    /**
     * Default Constructor
     * @param hadoopHost the Host List in Hash Map name and ip
     * @param pwd the encrypted password
     * @param usr the user name 
     */
    AppUtilsConfig(HashMap<String, String> hadoopHost, String pwd, String usr) {
        this.preprodHosts = hadoopHost;
        this.pwd = pwd;
        this.usr = usr;

    }

    //methods
    //setter
    /**
     * 
     * @param newPreprodHosts
     */
    public void setPreprodHosts(HashMap<String, String> newPreprodHosts) {
        preprodHosts = newPreprodHosts;
    }

    /**
     * 
     * @param new encrypted password
     */
    public void setPwd(String newPwd) {
        pwd = newPwd;
    }

    /**
     * 
     * @param newUsr user name
     */
    public void setUsr(String newUsr) {
        usr = newUsr;
    }

    //getter
    /**
     * 
     * @return hostname and ip
     */
    public HashMap<String, String> getPreprodHosts() {
        return preprodHosts;
    }

    /**
     * 
     * @return encrypted password
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * 
     * @return user name
     */
    public String getUsr() {
        return usr;
    }

}
