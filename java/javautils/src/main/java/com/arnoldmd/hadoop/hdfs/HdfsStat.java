/*
 * Copyright 2015 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 01/20/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/hdfs/HdfsStat.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: HdfsStat.java,v $
 * <Description>, ArnoldMD 01/20/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 01/20/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 01/20/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.hdfs;

import org.apache.log4j.Logger;

public class HdfsStat {


    private static final String APP_NAME = HdfsStat.class.getSimpleName();
    private static Logger log = Logger.getLogger(APP_NAME);

    int replicationFactor_;
    String owner_;
    String group_;
    private long fileSize_;
    private String fileDate_;
    private String fileName_;

    public HdfsStat(int replicationFactor, String owner, String group, long fileSize, String fileDate, String fileName) {
        replicationFactor_ = replicationFactor;
        owner_ = owner;
        group_ = group;
        fileSize_ = fileSize;
        fileDate_ = fileDate;
        fileName_ = fileName;
        log.debug("replicationFactor: " + replicationFactor + " ,owner: " + owner + " ,group: " + group + " ,fileSize: " + fileSize + " ,fileDate: " + fileDate + " ,fileName: " + fileName);
    }

    public String getOwner() {
        return owner_;
    }

    public String getGroup() {
        return owner_;
    }

    /**
     * @return the fileSize_
     */
    public long getFileSize_() {
        return fileSize_;
    }

    /**
     * @param fileSize_ the fileSize_ to set
     */
    public void setFileSize_(long fileSize_) {
        this.fileSize_ = fileSize_;
    }

    /**
     * @return the fileDate_
     */
    public String getFileDate_() {
        return fileDate_;
    }

    /**
     * @param fileDate_ the fileDate_ to set
     */
    public void setFileDate_(String fileDate_) {
        this.fileDate_ = fileDate_;
    }

    /**
     * @return the fileName_
     */
    public String getFileName_() {
        return fileName_;
    }

    /**
     * @param fileName_ the fileName_ to set
     */
    public void setFileName_(String fileName_) {
        this.fileName_ = fileName_;
    }
}
