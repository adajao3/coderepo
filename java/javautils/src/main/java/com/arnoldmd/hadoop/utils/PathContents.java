/*
 * Copyright 2014 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 12/10/2014   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/utils/PathContents.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: PathContents.java,v $
 * <Description>, ArnoldMD 12/10/2014   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 12/10/2014   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 12/10/2014   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.utils;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PathContents {

    private static final String APP_SVN_REV = "$Version: 1.3$" + " $Revision: 1.1 $";
    private static List<String> pathList_;
    private static List<String> fileList_;
    private static List<String> pathFileList_;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String ROOT = "C:\\Users\\adajao2\\Google Drive\\NetBeansProjects\\TestForJavaUtils\\dist";
        try {
            PathContents pc = new PathContents(ROOT);
            List<String> pathList = pc.getPathList();
            Collections.sort(pathList);
            for (String path : pathList) {
                System.out.println("Path:" + path);
            }

            List<String> fileList = pc.getFileList();
            Collections.sort(fileList);
            for (String file : fileList) {
                System.out.println("Path:" + file);
            }

            // TODO code application logic here
        } catch (IOException ex) {
            Logger.getLogger(PathContents.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PathContents(String sourcePath) throws IOException {
        pathList_ = new ArrayList();
        fileList_ = new ArrayList();
        pathFileList_ = new ArrayList();
        FileVisitor<Path> fileProcessor = new ProcessFile();
        Files.walkFileTree(Paths.get(sourcePath), fileProcessor);

    }

    public List<String> getPathList() {
        return pathList_;
    }

    public List<String> getFileList() {
        return fileList_;
    }

    public List<String> getPathAndFileList() {
        return pathFileList_;
    }

    private static final class ProcessFile extends SimpleFileVisitor<Path> {

        @Override
        public FileVisitResult visitFile(
                Path aFile, BasicFileAttributes aAttrs
        ) throws IOException {
            System.out.println("Processing file:" + aFile + APP_SVN_REV);
            fileList_.add(aFile.toString());
            pathFileList_.add(aFile.toString());
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(
                Path aDir, BasicFileAttributes aAttrs
        ) throws IOException {
            System.out.println("Processing directory:" + aDir);
            pathList_.add(aDir.toString());
            pathFileList_.add(aDir.toString());
            return FileVisitResult.CONTINUE;
        }
    }

}
