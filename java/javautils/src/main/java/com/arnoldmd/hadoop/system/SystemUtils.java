/*
 * Copyright 2014 ArnoldMD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 11/24/2014   Header: $Header:
 * /home/cvs/repository/JavaUtils/src/com/tpim/hadoop/Os/OsUtils.java,v 1.3
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    $Log: SystemUtils.java,v $
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:    Mulitple updates
 * 2014/12/09 18:25:49 arnoldmd Exp $    Log:     
 * Log:    Revision 1.3 2014/12/09 18:25:49 arnoldmd   Log:    updated Mail and
 * HDFS   Log:      Log:    Revision 1.2 2014/11/25 02:22:47 arnoldmd   Log:   
 * Update for HDFS utilities amd merging   Log:   
 */
package com.arnoldmd.hadoop.system;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

/**
 *
 * @author adajao2
 */
public class SystemUtils {

    private static String OS = null;

    /**
     * Retrieve the Operating System where the Application is executed
     * @return
     */
    public static String getOsName() {
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    /**
     * Check if executed in Windows
     * @return true if windows
     */
    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    /**
     * Check if executed if Linux
     * @return
     */
    public static boolean isLinux() {
        return getOsName().startsWith("Rhel");
    }

    /**
     *
     * @return
     */
    public static String getLogProp() {
        String logProp = "";
        if (getOsName().startsWith("Windows")) {
            logProp = "C:\\Temp\\logging.properties";
        } else {
            logProp = "/home/omc/staging/etl/config/logging.properties";

        }
        return logProp;
    }

    /**
     *
     * @return
     */
    public static String getAppProp() {
        String logProp = "";
        if (getOsName().startsWith("Windows")) {
            logProp = "C:\\Temp\\config\\App.properties";
        } else {
            logProp = "/home/omc/staging/etl/java/conf/App.properties";
        }
        return logProp;
    }

    /**
     *
     * @return
     */
    public static String getTjuConf() {
        String logProp = "";
        if (getOsName().startsWith("Windows")) {
            logProp = "C:\\Temp\\config\\";
        } else {
            logProp = "/home/omc/staging/etl/config/logging.properties";

        }
        return logProp;
    }

    /**
     *
     * @return
     */
    public static String getrefreshTableSql() {
        String logProp = "";
        if (getOsName().startsWith("Windows")) {
            logProp = "C:\\Temp\\preproduction_REFRESH_STATS.sql";
        } else {
            logProp = "/home/omc/staging/etl/metadata/tables/preproduction/preproduction_REFRESH_STATS.sql";

        }
        return logProp;
    }

    /**
     * 
     * @return
     */
    public static String getComputeStatsSql() {
        String logProp = "";
        if (getOsName().startsWith("Windows")) {
            logProp = "C:\\Temp\\preproduction_COMPUTE_STATS.sql";
        } else {
            logProp = "/home/omc/staging/etl/metadata/tables/preproduction/preproduction_COMPUTE_STATS.sql";
//              logProp = "/home/arnoldmd/development/java/preproduction_COMPUTE_STATS.sql";

        }
        return logProp;
    }

    public static void workAround() throws IOException {
        //start work around//
        File workaround = new File(".");
        System.getProperties().put("hadoop.home.dir", workaround.getAbsolutePath());
        new File("./bin").mkdirs();
        new File("./bin/winutils.exe").createNewFile();
        //* end work around//
    }

    /**
     * retrieve the process id
     * @return
     */
    public static String getPid() {
        RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
        return rt.getName().split("@")[0];
    }

    /**
     * retrieves host id
     * @return
     */
    public static String getHost() {
        RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
        return rt.getName().split("@")[1];
    }

    /**
     * retrieves user name
     * @return
     */
    public static String getUser() {
        return System.getProperty("user.name");

    }

}
