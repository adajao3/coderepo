/*
 * Copyright 2015 adajao2.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * <Description>, ArnoldMD 03/02/2015   Header: $Header: /home/cvs/repository/tpim-javautils-1.2.6/src/com/tpim/hadoop/zoomdata/ZoomDataLive.java,v 1.1 2015/03/25 23:55:07 arnoldmd Exp $    Log:    $Log: ZoomDataLive.java,v $
 * <Description>, ArnoldMD 03/02/2015   Header: $Header$    Log:    Revision 1.1  2015/03/25 23:55:07  arnoldmd
 * <Description>, ArnoldMD 03/02/2015   Header: $Header$    Log:    Mulitple updates
 * <Description>, ArnoldMD 03/02/2015   Header: $Header$    Log:   
 */
package com.arnoldmd.hadoop.zoomdata;
import com.arnoldmd.hadoop.date.TimeConversion;
import com.google.gson.Gson;

import static com.arnoldmd.hadoop.jsch.Jscht.sendCurl;
import org.apache.log4j.Logger;

public class ZoomDataLive {

    private static final String APP_SVN_REV = "$Version: 1.0$".replaceAll("\\$", "").replace("sion", "") + " $Revision: 1.1 $".replaceAll("\\$", "").replace("ision", "");
    private static final String APP_NAME = ZoomDataLive.class.getSimpleName();
    private static final String ZD_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static Logger log = Logger.getLogger(APP_NAME);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    public static String getOsName() {
        String OS = "";
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public static String zdStart(String host, String user, String appName, int pid, String ZoomDataTableName) {
        TpimAlarm ta = new TpimAlarm();
        TimeConversion tm = new TimeConversion();
        String updateTimeGMT = tm.getDateTimeGMT("yyyy-MM-dd HH:mm:ss");
        String runtimeTime = tm.getDateTime("yyyy-MM-dd HH:mm:ss");
        ta.setUpdateTime(updateTimeGMT);
        ta.setNode(host);
        ta.setUser(user);
        ta.setAppName(appName);
        ta.setRunning(1);
        ta.setPid(pid);
        ta.setLastRun(runtimeTime);
        ta.setErrorList("");
        Gson gson = new Gson();
        System.out.println("json: " + gson.toJson(ta));
        String result = sendCurl(ZoomDataTableName, gson.toJson(ta));
        return result;
    }

    public static String zdEnd(String host, String user, String appName, int pid, int duration, String runtime, String ZoomDataTableName) {
        TpimAlarm ta = new TpimAlarm();
        TimeConversion tm = new TimeConversion();
        String updateTimeGMT = tm.getDateTimeGMT("yyyy-MM-dd HH:mm:ss");
        String runtimeTime = tm.getDateTime("yyyy-MM-dd HH:mm:ss");
        ta.setUpdateTime(updateTimeGMT);
        ta.setNode(host);
        ta.setUser(user);
        ta.setAppName(appName);
        ta.setRunning(0);
        ta.setPid(pid);
        ta.setDuration(duration);
        ta.setLastRun(runtimeTime);
        ta.setErrorList("");
        Gson gson = new Gson();
        System.out.println("json: " + gson.toJson(ta));
        String result = sendCurl(ZoomDataTableName, gson.toJson(ta));
        return result;
    }

    public static String zdError(String host, String user, String appName, int pid, String ZoomDataTableName, String errorList) {
        TpimAlarm ta = new TpimAlarm();
        TimeConversion tm = new TimeConversion();
        String updateTimeGMT = tm.getDateTimeGMT("yyyy-MM-dd HH:mm:ss");
        String runtimeTime = tm.getDateTime("yyyy-MM-dd HH:mm:ss");
        ta.setUpdateTime(updateTimeGMT);
        ta.setNode(host);
        ta.setUser(user);
        ta.setAppName(appName);
        ta.setRunning(0);
        ta.setPid(pid);
        ta.setDuration(0);
        ta.setLastRun(runtimeTime);
        ta.setErrorList(errorList);

        Gson gson = new Gson();
        System.out.println("json: " + gson.toJson(ta));
        String result = sendCurl(ZoomDataTableName, gson.toJson(ta));
        return result;
    }
}
