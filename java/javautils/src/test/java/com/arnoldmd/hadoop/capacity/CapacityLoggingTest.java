/**
 * 
 */
package com.arnoldmd.hadoop.capacity;

import static org.junit.Assert.*;

import org.junit.Test;

import com.arnoldmd.hadoop.capacity.CapacityLogging;

/**
 * @author adajao2
 *
 */
public class CapacityLoggingTest {

	@Test
	public void test() {
		final String duration = "1.3";
		final String region = "SOUTH";
		final String operation = "DEDUP";
		final String adaptor = "TEST ADAPTOR";
		final String measurement = "TEST MEASUREMENT";
		final String object = "TEST MEASUREMENT";
		final String filesize = "123456";
		final String numrows = "123456";
		final String counters = "test counters";
		CapacityLogging caplog = new CapacityLogging();
		String caplogLine = caplog.getCapacityLine(duration, region, operation,
				adaptor, measurement, object, filesize, numrows, counters);
		String[] capacity = caplogLine.split(",");

		assertEquals(duration, capacity[3]);
		assertEquals(region, capacity[4]);
		assertEquals(operation, capacity[6]);
		assertEquals(adaptor, capacity[7]);
		assertEquals(measurement, capacity[8]);
		assertEquals(object, capacity[9]);
		assertEquals(filesize, capacity[10]);
		assertEquals(numrows, capacity[11]);
		assertEquals(counters, capacity[12]);

	}
}
