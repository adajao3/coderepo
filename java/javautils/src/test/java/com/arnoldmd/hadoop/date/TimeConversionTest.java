package com.arnoldmd.hadoop.date;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
/**
 * Tests for Time Conversion
 * @author adajao2
 *
 */
public class TimeConversionTest {

	TimeConversion tc;

	@Before
	public void setUp() throws Exception {
		tc = new TimeConversion();
	}

	@Test
	public void test() {

		assertEquals("20150130", tc.getDateTime("2015-01-30 12:00:01",
				"YYYY-MM-dd HH:mm:ss", "YYYYMMdd"));

		String dateStart = "2015-01-30 12:00:01";
		String dateStop = "2015-01-30 12:01:01";
		String dateFormat = "YYYY-MM-dd HH:mm:ss";
		assertEquals(60, tc.getRunTimeSec(dateStart, dateStop, dateFormat));

		long runtime = 1231214;
		assertEquals("00:20:31.21",tc.getTimeConversion(runtime));


	}

}
