/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arnoldmd.hadoop.capacity;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.PropertyUtils;

import com.arnoldmd.hadoop.utils.Adaptor;
import com.arnoldmd.hadoop.utils.ReadConfig;

/**
 *
 * @author adajao2
 */
public class test {

    public static void main(String[] args) {
        // TODO code application logic here
        getConfigMapTest();
//        beanUtil();
//        foundTest();
//        testConfigValue();
    }

    public static void beanUtil() {

        try {
            MobilePhone flexiColor = new MobilePhone();
            //here color and blue strings can come from variety or sources
            //e.g. configuration files, database, any upstream system or via HTTP Request
            PropertyUtils.setProperty(flexiColor, "hue", "blue");
            String value = (String) PropertyUtils.getProperty(flexiColor, "color");
            System.out.println("PropertyUtils Example property value: " + value);
            System.out.println("color: " + flexiColor.getColor());
            flexiColor.setHue("White");
            System.out.println("color: " + flexiColor.getColor());
            Object obj = PropertyUtils.getPropertyType(flexiColor, "price");
            System.out.println("PropertyUtils Example property value: " + obj.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void reflection() {
        // TODO code application logic here
        Adaptor syndrome = new Adaptor();

        Method[] methods = syndrome.getClass().getMethods();

        for (int index = 0; index < methods.length; index++) {

            if (methods[index].getName().contains("set")) {
                System.out.println(methods[index].getName());
                // Do something here

            }

        }
    }

    public static class MobilePhone {

        private String brand;
        private String color_;
        private int price_;

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setPrice(int price) {
            this.price_ = price;
        }

        public String getColor() {
            return color_;
        }

        public void setHue(String color1) {
            this.color_ = color1;
        }
    }

    public static void testConfigValue() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("HDFS_ROOT", "/user/omc/data");
        map.put("HDFS_DIR", "<SYSTEM_TABLESQL_DIR>/<SYSTEM_DATABASE>_COMPUTE_STATS.sql");
        map.put("SYSTEM_TABLESQL_DIR", "<SYSTEM_METADATA_DIR>/tables/<SYSTEM_DATABASE>");
        map.put("SYSTEM_METADATA_DIR", "/home/omc/staging/etl/metadata");
        map.put("SYSTEM_DATABASE", "preproduction");

        String confValue = map.get("HDFS_DIR");
        String Value = configValue(map, confValue);

        System.out.println("color: " + Value);
    }

//    public static String getConfigValue(Map<String, String> confMapAll, String confValue) {
//        System.out.println("color0: " + confValue);
////        String pattern = "(.*)<(.*)>(.*)";
//        Pattern fkPetan = Pattern.compile("(.*)<(.*)>(.*)");
//        Matcher m = fkPetan.matcher(confValue);
//        while (m.find()) {
//            String fkPetanVal = confMapAll.get(m.group(2));
//            String fkPetanVal2 = m.group(1) + fkPetanVal;
//            System.out.println("color2: " + fkPetanVal2);
//            confValue = getConfigValue(confMapAll, fkPetanVal2);
//            System.out.println("color3: " + confValue);
//        }
//        System.out.println("color4: " + confValue);
//        return confValue;
//
//    }
    public static String configValue(Map<String, String> confMapAll, String confValue) {
        System.out.println("confValue: " + confValue);
//        String pattern = "(.*)<(.*)>(.*)";
        Pattern fkPetan = Pattern.compile("(<\\w+>)");
        Matcher m = fkPetan.matcher(confValue);

        int count = 0;
        if (m.find()) {
            count++;
            System.out.println("found: " + count + " : "
                    + m.start() + " - " + m.end() + " " + m.group() + ": " + getFkPetan(m.group()));
            String fkPetanVal = confMapAll.get(getFkPetan(m.group()));
            System.out.println("color2: " + fkPetanVal);
            String confValue1 = m.replaceFirst(fkPetanVal);
            System.out.println("confValue: " + confValue1);
            confValue = configValue(confMapAll, confValue1);
        }

        return confValue;

    }

    public static String getFkPetan(String fkPetan) {
        System.out.println("fkPetan: " + fkPetan);
        Pattern pattern = Pattern.compile("^<(.*)>$");
        Matcher m = pattern.matcher(fkPetan);
        if (m.find()) {

            System.out.println("m.group " + m.group(1));
            return m.group(1);
        }
        return fkPetan;
    }

    public static void foundTest() {

        String text
                = "GMLCLBS911_HDFS_DIR";

//        if (text.contains("=")){
//             String[] strings = text.split("=");
//             for (String s: strings){
//                  System.out.println("foundtest s: " +  s) ;
//            }
//            
//        }
        String patternString = "(.*)_HDFS_DIR";

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);

        int count = 0;
        while (matcher.find()) {
            count++;
            System.out.println("foundtest1: " + count + " : "
                    + matcher.start() + " - " + matcher.end() + " - " + matcher.group(1));
        }
    }

    public static void getConfigMapTest() {
        String delimiter = ",";
        int valueCount = 3;
        String configFile = "C:\\Temp\\omes_tpim_name_mapping_v2_amd.csv";
        ReadConfig rc = new ReadConfig();
        try {
            rc.getConfigTpimMap(delimiter, valueCount, configFile);
        } catch (IOException ex) {
            Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
