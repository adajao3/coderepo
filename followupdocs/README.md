
# Follow Up Answers #

## 1. Red Shift Max No. Column ##
 * I stand corrected on my assumption that there is no column limit on redshift where infact there the limit is **1,600 Maximum Column**
  Redshift Maximum is 1600 but performance suffers beyond 400 column wide. 
  
* There are however other database that allows more column depending on use case/access patterns:
  	* For extremely wide columns use Cassandra up to 2 Billion column [Datastax Reference](https://docs.datastax.com/en/cql/3.1/cql/cql_reference/refLimits.html)
  	* Hbase theoretically unbounded column count

## 2. Tableau Filtering ##
* This can be done by using Actions -> Filtering it can be applied to a specific sheet or use for all sheets [tableau_kb](http://kb.tableau.com/articles/issue/applying-action-filter-to-worksheets-in-different-dashboards)

## Cloud Long Term Storage ##
 ### Assumptions: ###
* Monthly Max Data Size:  500TB
* Retention for 3 Months, 1 Year and 2 Years
* Should be Secured

 ### AWS Glacier ##
 ##Features:###
* $0.005/GB/Month
* 3-5 HR Data Retrieval
* Security and Regulatory Compliance  
	* Time based retention (Will not be deleted if retention set is not met even with admin rights)
	* Multi-Factor Authentication (MFA)
	* Fine Grain ACLs for each Vault
	* Locakble/Immutable Vault policy
	* Free Data Access upto 5% of Monthly Storage or  


[AWS Glacier.png](https://bitbucket.org/repo/5L4AXB/images/3859103553-AWS%20Glacier.png)

## Java Api for EMR ##

* Create MR, Hive, Spark Application JAR and save to S3
* From IDE using AWS SDK Create AWS Java Project
	* Provision EMR Cluster
	* Run MR2, Hive, Spark using Jar in S3 
	* Make sure you make S3 as output directory or Copy HDFS output to S3 before you shutdown cluster
* Output should be available in S3
